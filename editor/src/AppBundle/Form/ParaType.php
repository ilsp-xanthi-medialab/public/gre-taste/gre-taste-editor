<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\CorpusDocumentParagraph;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class ParaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('category', TextType::class, [
                    'required' => false,
                    'label' => 'κατηγορία',
                ])

                ->add('name', TextType::class, [
                    'required' => true,
                    'label' => 'όνομα',
                ])
                ->add('description', TextareaType::class, [
                    'required' => false,
		    'label' => 'περιγραφή',
		    'attr' => array('class' => 'textarea5lines')
                ])
                ->add('concept', EntityType::class, [
                    'required' => false,
                    'class' => 'AppBundle\Entity\Term',
                    'choice_label' => 'path',
                    'label' => 'Έννοια',
                    'attr' => array('data-widget' => 'select2')
                ])

        ;
        //data-widget="select2"
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => CorpusDocumentParagraph::class,
        ));
    }

    public function getBlockPrefix() {
        return 'ParaType';
    }

}
