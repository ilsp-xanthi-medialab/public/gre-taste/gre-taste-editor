<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\SkosNote;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class SkosNoteType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('noteType', EntityType::class, [
                    'required' => true,
                    'class' => 'AppBundle\Entity\SkosNoteType',
                    'choice_label' => 'value',
                    'label' => 'Τύπος',
                ])
                ->add('noteLanguage', EntityType::class, [
                    'required' => true,
                    'class' => 'AppBundle\Entity\SkosLanguage',
                    'choice_label' => 'name',
                    'label' => 'Γλώσσα',
                ])
                ->add('noteValue', TextareaType::class, [
                    'required' => true,
                    'label' => 'Τιμή',
                ])
                ->add('noteURL', TextareaType::class, [
                    'required' => true,
                    'label' => 'Τιμή (URL)',
                ])
                ->add('user', EntityType::class, [
                    'required' => false,
                    'class' => 'AppBundle\Entity\User',
                    'choice_label' => 'username',
                    'label' => 'User',
                ])
        ;
        //data-widget="select2"
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => SkosNote::class,
        ));
    }

    public function getBlockPrefix() {
        return 'SkosNoteType';
    }

}
