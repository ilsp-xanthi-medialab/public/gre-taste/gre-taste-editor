<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\ConceptLabel;

use AppBundle\Entity\Source;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of ConceptType
 *
 * @author pminos
 */
class ConceptType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
               
                ->add('head', TextType::class, [
                    'required' => true,
                    'label' => 'Κεφαλή',
                ])
              

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Term::class,
        ));
    }

    public function getBlockPrefix() {
        return 'ConceptType';
    }

}
