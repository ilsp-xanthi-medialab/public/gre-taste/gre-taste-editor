<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\SkosRelation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class SkosRelationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('relationType', EntityType::class, [
                    'required' => true,
                    'class' => 'AppBundle\Entity\SkosRelationType',
                    'choice_label' => 'value',
                    'label' => 'Τύπος',
                ])
                ->add('relatedTerm', EntityType::class, [
                    'required' => false,
                    'class' => 'AppBundle\Entity\Term',
                    'choice_label' => 'path',
                    'label' => 'Έννοια',
                    'attr' => array('data-widget' => 'select2')
                ])
                ->add('noteURL', TextareaType::class, [
                    'required' => true,
                    'label' => 'Έννοια (URL)',
                ])

        ;
        //data-widget="select2"
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => SkosRelation::class,
        ));
    }

    public function getBlockPrefix() {
        return 'SkosRelationType';
    }

}
