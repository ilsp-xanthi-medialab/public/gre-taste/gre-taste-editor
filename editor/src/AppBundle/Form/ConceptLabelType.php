<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\ConceptLabel;

use AppBundle\Entity\Source;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class ConceptLabelType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('noteType', EntityType::class, [
                    'required' => true,
                    'class' => 'AppBundle\Entity\SkosLabelType',
                    'choice_label' => 'value',
                    'label' => 'Τύπος',
                ])
                ->add('noteValue', TextType::class, [
                    'required' => true,
                    'label' => 'Τιμή',
                ])
                ->add('noteLanguage', EntityType::class, [
                    'required' => true,
                    'class' => 'AppBundle\Entity\SkosLanguage',
                    'choice_label' => 'name',
                    'label' => 'Γλώσσα',
                ])
                ->add('article', EntityType::class, [
                    'required' => false,
                    'class' => 'AppBundle\Entity\Article',
                    'choice_label' => 'value',
                    'label' => 'άρθρο',
                ])                
                ->add('transcript', TextType::class, [
                    'required' => false,
                    'label' => 'Μεταγραφή',
                ])              
                //->add('characterizations', EntityType::class, array(
                //    'class' => 'AppBundle:Characterization',
                //    'label' => 'Χαρακτηρισμός',
                //    'choice_label' => 'value',
                //    'multiple' => true,
                //    'expanded' => false,
                //    'attr' => array('data-widget' => 'select2')
                //))
                ->add('termSources', CollectionType::class, array(
					'required' => false,
                    'label' => 'Πηγές',
                    'by_reference' => false,
                    'entry_type' => LabelSourceType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'attr' => array(
                        'class' => 'label-source-selector',
                    ),
                

                ))                
                //->add('noteURL', TextType::class, [
                //    'required' => true,
                //    'label' => 'value (URL)',
                //])

        ;
        //data-widget="select2"
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => ConceptLabel::class,
        ));
    }

    public function getBlockPrefix() {
        return 'ConceptLabelType';
    }

}
