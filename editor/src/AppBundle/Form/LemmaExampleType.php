<?php

namespace AppBundle\Form;

use AppBundle\Entity\LemmaExample;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LemmaExampleType
 *
 * @author pminos
 */
class LemmaExampleType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, [
                    'required' => true,
                    'label' => 'name',
                ])
                ->add('article', EntityType::class, array(
                    'class' => 'AppBundle:Article',
                    'choice_label' => 'value',
                ))
                ->add('source', TextType::class, [
                    'required' => true,
                    'label' => 'source',
                ])
                ->add('url', TextType::class, [
                    'required' => true,
                    'label' => 'url',
                ])
//                ->add('language', ChoiceType::class, array(
//                    'expanded' => 'true',
//                    'multiple' => 'true',
//                    'mapped' => false,
//                    'choices' => array(
//                        'English' => 'en',
//                        'Spanish' => 'es',
//                        'Bork' => 'muppets',
//                        'Pirate' => 'arr',
//                        'English1' => 'en1',
//                        'Spanish1' => 'es1',
//                        'Bork1' => 'muppets1',
//                        'Pirate1' => 'arr1',                        
//            )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => LemmaExample::class,
        ));
    }

    public function getBlockPrefix() {
        return 'LemmaExampleType';
    }

}
