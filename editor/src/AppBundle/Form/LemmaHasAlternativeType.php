<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\AlternativeTerm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class LemmaHasAlternativeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, [
                    'required' => true,
                    'label' => 'όρος',
                ])
                 ->add('transcript', TextType::class, [
                    'required' => false,
                    'label' => 'Μεταγραφή',
                ])
                ->add('article', EntityType::class, array(
                    'class' => 'AppBundle:Article',
                    'label' => 'άρθρο',
                    'choice_label' => 'value',
                ))
                ->add('characterizations', EntityType::class, array(
                    'class' => 'AppBundle:Characterization',
                    'label' => 'Χαρακτηρισμός',
                    'choice_label' => 'value',
                    'multiple' => true,
                    'expanded' => false,
                    'attr' => array('data-widget' => 'select2')
                ))
                ->add('termSources', EntityType::class, array(
                    'class' => 'AppBundle:Source',
                    'label' => 'Πηγές',
                    'choice_label' => 'value',
                    'multiple' => true,
                    'expanded' => false,
                    'attr' => array('data-widget' => 'select2')
                    
                ))                
                ->add('internalNotes', TextareaType::class, [
                    'required' => false,
                    'label' => 'internalNote',
                ])                
        ;
        //data-widget="select2"
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => AlternativeTerm::class,
        ));
    }

    public function getBlockPrefix() {
        return 'LemmaHasAlternativeType';
    }

}
