<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\LabelSource;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class LabelSourceType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
             
                ->add('value', EntityType::class, [
                    'required' => false,
                    'class' => 'AppBundle\Entity\Source',
                    'choice_label' => 'value',
                    'label' => 'Πηγή',
                ])
                ->add('url', TextareaType::class, [
                    'required' => true,
                    'label' => 'value (URL)',
                ])

        ;
        //data-widget="select2"
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => LabelSource::class,
        ));
    }

    public function getBlockPrefix() {
        return 'LabelSourceType';
    }

}
