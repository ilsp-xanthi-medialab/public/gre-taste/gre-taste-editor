<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Form;

use AppBundle\Entity\LemmaHasArticle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of LemmaHasArticleType
 *
 * @author pminos
 */
class LemmaHasArticleType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('article', EntityType::class, array(
//                    'expanded' => 'false',
//                    'multiple' => 'false',
                    'class' => 'AppBundle:Article',
                    'choice_label' => 'value',
                        //'mapped' => false,
//                    'choices' => array(
//                        'English' => 'en',
//                        'Spanish' => 'es',
//                        'Bork' => 'muppets',
//                        'Pirate' => 'arr',
//                        'English1' => 'en1',
//                        'Spanish1' => 'es1',
//                        'Bork1' => 'muppets1',
//                        'Pirate1' => 'arr1',
//                    )
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => LemmaHasArticle::class,
        ));
    }

    public function getBlockPrefix() {
        return 'LemmaHasArticleType';
    }

}
