<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use DateTime;
use Doctrine\ORM\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AdminController extends BaseAdminController {

    public function createNewUserEntity() {

        return $this->get('fos_user.user_manager')->createUser();
    }

    public function prePersistUserEntity($user) {

        $this->get('fos_user.user_manager')->updateUser($user, false);
    }

    public function preUpdateUserEntity($user) {

        $this->get('fos_user.user_manager')->updateUser($user, false);
    }

    //public function persistConceptEntity($user) {
    //}
    //public function updateConceptEntity($user) {
    //}

                    
    public function createRelationTypeEntityFormBuilder($entity, $view) {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $fields = $formBuilder->all();
        /**
         * @var  $fieldId string
         * @var  $field FormBuilder
         */
        foreach ($fields as $fieldId => $field) {
            if ($fieldId == 'domainFacet1'||$fieldId == 'domainFacet2'||$fieldId == 'domainFacet3'
                    ||$fieldId == 'rangeFacet1'||$fieldId == 'rangeFacet2'||$fieldId == 'rangeFacet3'
                    ) {

                $options = [
                    'attr' => ['size' => 1,],
                    'required' => false,
                    'multiple' => false,
                    'expanded' => false,
                    'class' => 'AppBundle\Entity\Term',
                ];
                $options['query_builder'] = function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('e');

                    return $qb->where($qb->expr()->eq('e.isFacet', '1'))
                                    
                                    
                                    
                                    ->orderBy('e.id', 'ASC');
                };
                $formBuilder->add($fieldId, EntityType::class, $options);
            }
        }

        return $formBuilder;
    }

}

?>
