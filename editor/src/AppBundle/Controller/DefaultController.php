<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AppBundle\Entity\Term;
use AppBundle\Entity\SearchQuery;
use AppBundle\Entity\ConceptLabel;
use AppBundle\Form\SearchQueryType;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        return $this->render('default/homepage.html.twig');
    }

    /**
     * @Route("/annotator", name="annotator")
     */
    public function annotatorAction(Request $request) {
        return $this->render('default/annotator.html.twig');
    }

    /**
     * @Route("/admin/backup_db", name="backup_db")
     */
    public function backupAction(Request $request) {
        $out = shell_exec('mysqldump -u' . $this->getParameter('database_user') . ' -p' . $this->getParameter('database_password') . ' ' . $this->getParameter('database_name'));
        $response = new Response($out, Response::HTTP_OK, array('content-type' => 'text/plain'));

        $disposition = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'gretaste-' . date('YmdHis') . '.sql'
        );

        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }

    /**
     * @Route("/admin/clear", name="clear_cache")
     */
    public function clearAction(Request $request) {
        $out = shell_exec('php ../bin/console cache:clear -e prod');
        $out .= shell_exec('php ../bin/console cache:warmup -e prod');
        $response = new Response(
                $out,
                Response::HTTP_OK,
                array('content-type' => 'text/plain')
        );
        return $response;
    }

    /**
     * @Route("/concepts/search", name="concepts_search")
     */
    public function searchAction(Request $request) {
        $q = new SearchQuery();
        $form = $this->createForm(SearchQueryType::class, $q);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $q = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($task);
            // $entityManager->flush();
            //return $this->redirectToRoute('task_success');
        }

        return $this->render('default/search.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/concepts/list", name="concepts_list")
     */
    public function listAction(Request $request) {
        return $this->render('default/homepage.html.twig');
    }

    /**
     * @Route("/concepts/delete/{id}", name="deleteConceptAction")
     */
    public function deleteConceptAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concept = $repository->find($id);
        if ($concept->getChildren()->isEmpty()) {
            $response = new JsonResponse();
            return $response;
        } else {
            throw $this->createNotFoundException('The entity does not exist');
        }
    }


    /**
     * @Route("/dictionary", name="getAllDictionaryAsTextAction")
     */
    public function getAllDictionaryAsTextAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(ConceptLabel::class);
        $array = [];
        $entities = $repository->findAll();
        foreach ($entities as $e) {
            if ($e->getNoteValue())
                $array[] = $e->getNoteValue();
        }
        $response = new JsonResponse($array);
        return $response;
    }

    

    /**
     * @Route("/dictionary/{lang}", name="getDictionaryAsTextAction")
     */
    public function getDictionaryAsTextAction(Request $request, $lang) {
        $repository = $this->getDoctrine()->getRepository(ConceptLabel::class);
        $array = [];
        $entities = $repository->findAll();
        foreach ($entities as $e) {
            if ($e->getNoteLanguage() && $e->getNoteLanguage()->getValue() === $lang)
                if ($e->getNoteValue())
                    $array[] = $e->getNoteValue();
        }
        $response = new JsonResponse($array);
        return $response;
    }

   

}
