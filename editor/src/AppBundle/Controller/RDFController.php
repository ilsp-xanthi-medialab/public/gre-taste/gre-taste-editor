<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AppBundle\Entity\Term;
use AppBundle\Entity\SearchQuery;
use AppBundle\Entity\ConceptLabel;
use AppBundle\Form\SearchQueryType;

class RDFController extends Controller {
    //https://www.w3.org/2001/sw/Europe/reports/thes/1.0/guide/20040504/#2

    /**
     * @Route("/rdf/concept/{id}", name="getConceptAsRDFAction")
     */
    public function getConceptAsRDFAction(Request $request, $id) {
        $s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\">\n";

        $repository = $this->getDoctrine()->getRepository(Term::class);
        $e = $repository->find($id);

        $s .= sprintf("<skos:Concept rdf:about=\"http://gretaste.ilsp.gr/rdf/concept/%d\">\n", $e->getId());

        foreach ($e->getSkosLabels() as $l) {
            if (empty($l->getNoteValue())) {
                continue;
            }
            if (empty($l->getNoteType())) {
                continue;
            }
            if (empty($l->getNoteType()->getMapTo())) {
                continue;
            }
            if (empty($l->getNoteLanguage())) {
                continue;
            }
            $s .= sprintf("<%s xml:lang=\"%s\">%s</%s>\n",
                    $l->getNoteType()->getMapTo(), $l->getNoteLanguage()->getValue(), $l->getNoteValue(), $l->getNoteType()->getMapTo());
        }

        foreach ($e->getSkosRelations() as $l) {
            if (empty($l->getRelationType())) {
                continue;
            }
            if (empty($l->getRelationType()->getMapTo())) {
                continue;
            }
            if (!empty($l->getRelatedTerm())) {
                $s .= sprintf("<%s rdf:resource=\"http://gretaste.ilsp.gr/rdf/concept/%d\"/>\n",
                        $l->getRelationType()->getMapTo(), $l->getRelatedTerm()->getId());
            } else if (!empty($l->getNoteURL())) {
                $s .= sprintf("<%s rdf:resource=\"%s\"/>\n",
                        $l->getRelationType()->getMapTo(), $l->getNoteURL());
            }
        }

        foreach ($e->getSkosNotes() as $l) {
            if (!empty($l->getNoteType()->getMapTo())) {

                if ($l->getNoteLanguage()) {
                    if (!empty($l->getNoteValue())) {
                        $s .= sprintf("<%s xml:lang=\"%s\">%s</%s>\n",
                                $l->getNoteType()->getMapTo(), $l->getNoteLanguage()->getValue(), $l->getNoteValue(), $l->getNoteType()->getMapTo()
                        );
                    } else if (!empty($l->getNoteURL())) {
                        $s .= sprintf("<%s xml:lang=\"%s\" rdf:resource=\"%s\"></%s>\n",
                                $l->getNoteType()->getMapTo(), $l->getNoteLanguage()->getValue(), $l->getNoteURL(), $l->getNoteType()->getMapTo()
                        );
                    }
                } else {
                    if (!empty($l->getNoteValue())) {
                        $s .= sprintf("<%s>%s</%s>\n",
                                $l->getNoteType()->getMapTo(), $l->getNoteValue(), $l->getNoteType()->getMapTo()
                        );
                    } else if (!empty($l->getNoteURL())) {
                        $s .= sprintf("<%s rdf:resource=\"%s\"></%s>\n",
                                $l->getNoteType()->getMapTo(), $l->getNoteURL(), $l->getNoteType()->getMapTo()
                        );
                    }
                }
            }
        }

        $s .= "</skos:Concept>\n";

        $s .= '</rdf:RDF>';
        $response = new Response($s, Response::HTTP_OK, ['content-type' => 'application/rdf+xml']);
        return $response;
    }

    /**
     * @Route("/rdf/dictionary.rdf", name="getAllDictionaryAsRDFAction")
     */
    public function getAllDictionaryAsRDFAction(Request $request) {
        $s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\">\n";

        $repository = $this->getDoctrine()->getRepository(Term::class);
        $entities = $repository->findAll();
        foreach ($entities as $e) {

            $s .= sprintf("<skos:Concept rdf:about=\"http://gretaste.ilsp.gr/rdf/concept/%d\">\n", $e->getId());


            foreach ($e->getSkosLabels() as $l) {
                if (empty($l->getNoteValue())) {
                    continue;
                }
                if (empty($l->getNoteType())) {
                    continue;
                }
                if (empty($l->getNoteType()->getMapTo())) {
                    continue;
                }
                if (empty($l->getNoteLanguage())) {
                    continue;
                }
                $s .= sprintf("<%s xml:lang=\"%s\">%s</%s>\n",
                        $l->getNoteType()->getMapTo(), $l->getNoteLanguage()->getValue(), $l->getNoteValue(), $l->getNoteType()->getMapTo());
            }

            foreach ($e->getSkosRelations() as $l) {
                if (empty($l->getRelationType())) {
                    continue;
                }
                if (empty($l->getRelationType()->getMapTo())) {
                    continue;
                }
                if (!empty($l->getRelatedTerm())) {
                    $s .= sprintf("<%s rdf:resource=\"http://gretaste.ilsp.gr/rdf/concept/%d\"/>\n",
                            $l->getRelationType()->getMapTo(), $l->getRelatedTerm()->getId());
                } else if (!empty($l->getNoteURL())) {
                    $s .= sprintf("<%s rdf:resource=\"%s\"/>\n",
                            $l->getRelationType()->getMapTo(), $l->getNoteURL());
                }
            }

            foreach ($e->getSkosNotes() as $l) {
                if (!empty($l->getNoteType()))
                    if (!empty($l->getNoteType()->getMapTo())) {

                        if ($l->getNoteLanguage()) {
                            if (!empty($l->getNoteValue())) {
                                $s .= sprintf("<%s xml:lang=\"%s\">%s</%s>\n",
                                        $l->getNoteType()->getMapTo(), $l->getNoteLanguage()->getValue(), $l->getNoteValue(), $l->getNoteType()->getMapTo()
                                );
                            } else if (!empty($l->getNoteURL())) {
                                $s .= sprintf("<%s xml:lang=\"%s\" rdf:resource=\"%s\"></%s>\n",
                                        $l->getNoteType()->getMapTo(), $l->getNoteLanguage()->getValue(), $l->getNoteURL(), $l->getNoteType()->getMapTo()
                                );
                            }
                        } else {
                            if (!empty($l->getNoteValue())) {
                                $s .= sprintf("<%s>%s</%s>\n",
                                        $l->getNoteType()->getMapTo(), $l->getNoteValue(), $l->getNoteType()->getMapTo()
                                );
                            } else if (!empty($l->getNoteURL())) {
                                $s .= sprintf("<%s rdf:resource=\"%s\"></%s>\n",
                                        $l->getNoteType()->getMapTo(), $l->getNoteURL(), $l->getNoteType()->getMapTo()
                                );
                            }
                        }
                    }
            }

            $s .= "</skos:Concept>\n";
        }
        $s .= '</rdf:RDF>';
        $response = new Response($s, Response::HTTP_OK, ['content-type' => 'application/rdf+xml']);
        return $response;
    }

}
