<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SkosRelation;
use AppBundle\Entity\Term;
use AppBundle\Entity\Translation;
use AppBundle\Entity\Wordform;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RestController extends Controller {

    /**
     * @Route("/rest/path/{id}", name="rest_get_concept_parents")
     */
    public function getConceptParents2Action(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $response = new JsonResponse();
        $result = array();

        //$result[] = $id;
        //$id = $request->query->get('id');
        while (1) {

            $concept = $repository->find($id);
            if (!$concept->getParent())
                break;
            $id = $concept->getParent()->getId();
            $result[] = ['id' => $id, 'name' => $concept->getParent()->getHead()];
        }
        $result = array_reverse($result);

        $response->setData($result);
        return $response;
    }

    public function getConceptParents($id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $result = array();
        while (1) {
            $concept = $repository->find($id);
            if (!$concept->getParent())
                break;
            $id = $concept->getParent()->getId();
            $result[] = $id;
        }
        $result = array_reverse($result);
        return $result;
    }

    /**
     * @Route("/rest/concept/{id}", name="rest_get_concept")
     */
    public function showJSONConceptAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concept = $repository->find($id);
        if ($concept == null) {
            throw $this->createNotFoundException('The entity does not exist');
        } else {
            $genericObject = new stdClass;
            $genericObject->id = $concept->getId();
            if ($concept->getParent()) {
                $genericObject->parent = $concept->getParent()->getId();
            }
            $genericObject->path = $this->getConceptParents($id);
            $genericObject->labels = [];

            foreach ($concept->getSkosLabels() as $l) {
                if ($l->getNoteValue() && $l->getNoteType() && $l->getNoteLanguage()) {
                    $ol = new stdClass;
                    $ol->label = $l->getNoteValue();
                    $ol->lang = $l->getNoteLanguage()->getValue();
                    $ol->type = $l->getNoteType()->getValue();
                    $ol->typeId = $l->getNoteType()->getId();
                    $genericObject->labels [] = $ol;
                }
            }
            foreach ($concept->getSkosRelations() as $l) {
                if ($l->getRelationType() && $l->getRelatedTerm()) {
                    $ol = new stdClass;
                    //$ol->inherited = false;
                    $ol->related = $l->getRelatedTerm()->getId();
                    //$ol->lang = $l->getNoteLanguage()->getValue();
                    $ol->type = $l->getRelationType()->getValue();
                    $ol->typeId = $l->getRelationType()->getId();
                    $ol->category = $l->getRelationType()->getCategory();
                    $genericObject->relations [] = $ol;
                }
            }
            $repository2 = $this->getDoctrine()->getRepository(SkosRelation::class);

            $linked = $repository2->findBy(['relatedTerm' => $concept]);
            foreach ($linked as $l) {
                if ($l->getRelationType()) {
                    $ol = new stdClass;
                    $ol->related = $l->getTerm()->getId();
                    $ol->type = $l->getRelationType()->getValue();
                    $ol->typeId = $l->getRelationType()->getId();
                    $ol->category = $l->getRelationType()->getCategory();
                    $genericObject->reverse_relations [] = $ol;
                }
            }
            foreach ($concept->getChildren() as $child) {
                $ol = new stdClass;
                $ol->id = $child->getId();
                $ol->name = $child->getHead();
                $genericObject->imm_children [] = $ol;
            }

            //dump($linked);



            foreach ($concept->getSkosNotes() as $l) {
                if ($l->getNoteValue() && $l->getNoteType()
                	// && $l->getNoteLanguage()
                	) {
                    $ol = new stdClass;
                    $ol->label = $l->getNoteValue();
                    if($l->getNoteLanguage())
                    $ol->lang = $l->getNoteLanguage()->getValue();
                    
                    
                    $ol->type = $l->getNoteType()->getValue();
                    $ol->typeId = $l->getNoteType()->getId();
                    $ol->category = $l->getNoteType()->getCategory();
                    $genericObject->notes [] = $ol;
                }
            }
            $visited = [];
            $inherited = $this->getParentRelations($concept, $visited);

            foreach ($inherited as $l) {
                if ($l->getRelationType() && $l->getRelatedTerm()) {
                    $ol = new stdClass;
                    $ol->related = $l->getRelatedTerm()->getId();
                    //$ol->lang = $l->getNoteLanguage()->getValue();
                    $ol->type = $l->getRelationType()->getValue();
                    $ol->typeId = $l->getRelationType()->getId();
                    $ol->category = $l->getRelationType()->getCategory();
                    //$ol->inherited = true;
                    if ($this->contains($genericObject->relations, $ol))
                        continue;
                    $genericObject->relations [] = $ol;
                }
            }
            $response = new JsonResponse($genericObject);
            return $response;
        }
        $response = new JsonResponse();
        return $response;
    }

    public function contains($relations, $relation) {
        foreach ($relations as $ol) {
            if ($ol->typeId === $relation->typeId && $ol->type === $relation->type && $ol->related === $relation->related)
                return true;
        }
        return false;
    }

    public function getParentRelations($concept, &$visited) {

        if (!$concept) {
            return [];
        }
        if (in_array($concept->getId(), $visited)) {
            return [];
        }
        $visited[] = $concept->getId();

        $cr = $concept->getSkosRelations()->toArray();
        $ar = array_merge([], $cr);
        $parent = $concept->getParent();
        if ($parent) {
            $ar = array_merge($ar, $this->getParentRelations($parent, $visited));
        }
        foreach ($cr as $r) {
            if ($r->getRelationType() && $r->getRelationType()->getIsHierarchical()) {
                $n = $this->getParentRelations($r->getRelatedTerm(), $visited);
                $ar = array_merge($ar, $n);
            }
        }


        return $ar;
    }

    /**
     * @Route("/rest/search", name="rest_search")
     */
    public function rest_searchAction(Request $request) {
        $term = $request->query->get('term');
        $op = $request->query->get('op');
        $pw = '';
        $sw = '';
        if ($op === 'starts') {
            $sw = '%';
        }
        if ($op === 'ends') {
            $pw = '%';
        }
        if ($op === 'contains') {
            $pw = '%';
            $sw = '%';
        }

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Term');
        $query = $repository->createQueryBuilder('p')
                ->where('p.head LIKE :term')
                ->setParameter('term', $pw . $term . $sw)
                ->getQuery();
        $entities = $query->getResult();

        $array = [];
        foreach ($entities as $e) {

            $array[$e->getId()] = ['id' => $e->getId(),
                'path' => $this->getConceptParents($e->getId()),
                'name' => $e->getHead()];
        }

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:ConceptLabel');
        $query = $repository->createQueryBuilder('p')
                ->where('p.noteValue LIKE :term')
                ->setParameter('term', $pw . $term . $sw)
                ->getQuery();
        $entities = $query->getResult();
        foreach ($entities as $e) {

            $array[$e->getTerm()->getId()] = ['id' => $e->getTerm()->getId(),
                'path' => $this->getConceptParents($e->getTerm()->getId()),
                'name' => $e->getTerm()->getHead()
            ];
        }
        foreach ($array as $id => $entry) {
            $address = $this->fetch_address($id);
            if (count($address))
                $array[$id]['address'] = $address;
        }
        $response = new JsonResponse($array);
        return $response;
    }

    private function fetch_names() {
        $sql = "SELECT DISTINCT name FROM corpus_document_paragraph p where p.concept_id IS NULL";
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_COLUMN);
    }

    private function fetch_address($id) {
        $sql = "SELECT DISTINCT d.address as address FROM corpus_document_paragraph p LEFT JOIN corpus_document d ON d.id = p.document_id WHERE address IS NOT NULL AND is_valid = 1 AND concept_id = :cid";
        $params['cid'] = $id;
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_COLUMN);
    }

    private function update_para($id, $name) {
        $sql = "UPDATE corpus_document_paragraph p SET concept_id = :cid WHERE name = :name";
        $params['cid'] = $id;
        $params['name'] = $name;
        $stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
        $stmt->execute($params);
    }

    /**
     * @Route("/rest/tsearch", name="add_concept_to_corpus_para")
     */
    public function add_concept_to_corpus_para(Request $request) {

        $names = $this->fetch_names();
        $s[] = "e:" . count($names);
        $count = 0;
        $nf = 0;
        $skip = 0;
        foreach ($names as $term) {
            set_time_limit(20);
            $skip++;
            //if($skip <119000) continue;

            $pw = '%';
            $sw = '%';
            $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Term');
            $query = $repository->createQueryBuilder('p')
                    ->where('p.head LIKE :term')
                    ->setParameter('term', $pw . $term . $sw)
                    ->getQuery();
            $entities = $query->getResult();

            $array = [];
            foreach ($entities as $e) {

                $array[$e->getId()] = ['id' => $e->getId(),
                    'path' => $this->getConceptParents($e->getId()),
                    'name' => $e->getHead()];
            }

            $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:ConceptLabel');
            $query = $repository->createQueryBuilder('p')
                    ->where('p.noteValue LIKE :term')
                    ->setParameter('term', $pw . $term . $sw)
                    ->getQuery();
            $entities = $query->getResult();
            foreach ($entities as $e) {
                $array[$e->getTerm()->getId()] = ['id' => $e->getTerm()->getId(),
                    'path' => $this->getConceptParents($e->getTerm()->getId()),
                    'name' => $e->getTerm()->getHead()
                ];
            }

            $concept_id = -1;
            foreach ($array as $id => $entry) {
                if (count($entry['path']) && $entry['path'][0] == 497) {
                    $concept_id = $id;
                    //$s[] = "e1:" . $id;
                    break;
                }
            }
            if ($concept_id == -1) {
                foreach ($array as $id => $entry) {
                    $concept_id = $id;
                    //$s[] = "e2:" . $id;
                    break;
                }
            }
            if ($concept_id != -1) {
                //$s[] = "e3:" . $id;
                $this->update_para($id, $term);
                $count++;
            } else {
                $nf++;
            }
            if ($count > 100)
                break;
        }
        $s[] = "nf:" . $nf;

        $response = new JsonResponse($s);
        return $response;
    }

    /**
     * @Route("/dictionary.json", name="getAllDictionaryAsJSONAction")
     */
    public function getAllDictionaryAsJSONAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $array = [];
        $entities = $repository->findAll();
        foreach ($entities as $e) {
            $genericObject = new stdClass;
            $genericObject->id = $e->getId();
            $genericObject->labels = [];
            $array[] = $genericObject;

            foreach ($e->getSkosLabels() as $l) {
                if ($l->getNoteValue() && $l->getNoteType() && $l->getNoteLanguage()) {
                    $ol = new stdClass;
                    $ol->label = $l->getNoteValue();
                    $ol->lang = $l->getNoteLanguage()->getValue();
                    $ol->type = $l->getNoteType()->getValue();
                    $genericObject->labels [] = $ol;
                }
            }
        }
        $response = new JsonResponse($array);
        return $response;
    }


    /**
     * @Route("/dictionary.general.json", name="getAllGDictionaryAsJSONAction")
     */
    public function getAllGDictionaryAsJSONAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Translation::class);
        $array = [];
        $entities = $repository->findAll();
        foreach ($entities as $e) {
            $o = new stdClass;
            $o->id = $e->getId();
            $o->source = $e->getGreek();
            $o->target = $e->getTranslation();
            $array[] = $o;

            if ($e->getLanguage()) {
                $o->lang = $e->getLanguage()->getValue();
            }
        }

        $response = new JsonResponse($array);
        return $response;
    }
    
    /**
     * @Route("/morphology.json", name="getMorphologyAction")
     */
    public function getMorphologyAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Wordform::class);
        $array = [];
        $entities = $repository->findAll();
        foreach ($entities as $e) {
            $o = new stdClass;
            $o->t = $e->getTag();
            $o->w = $e->getWord();
            $o->l = $e->getLemma();
            $array[] = $o;
        }
        $response = new JsonResponse($array);
        return $response;
    }

    /**
     * @Route("/ambig.csv", name="getAmbigAction")
     */
    public function getAmbigAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Translation::class);
        $entities = $repository->findAll();

        $fr = [];

        foreach ($entities as $e) {
            $lang = 'none';
            if ($e->getLanguage()) {
                $lang = $e->getLanguage()->getValue();
            }
            if (!array_key_exists($lang, $fr)) {
                $fr[$lang] = [];
            }
            if (!array_key_exists($e->getGreek(), $fr[$lang])) {
                $fr[$lang][$e->getGreek()] = 1;
            } else {
                $fr[$lang][$e->getGreek()] = 1 + $fr[$lang][$e->getGreek()];
            }
        }

        foreach ($fr as $k => $v) {
            foreach ($v as $g => $e) {
                if($e === 1) {
                    unset($fr[$k][$g]);
                }
            }
        }
        $r = "";
        foreach ($fr as $k => $v) {
            foreach ($v as $g => $e) {
                    $r .= $k . "\t" . $g . "\t" . $e . "\n";
            }
        }
        
        $response = new Response($r);
        return $response;
    }

}
