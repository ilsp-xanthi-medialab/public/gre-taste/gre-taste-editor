<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AppBundle\Entity\Term;
use AppBundle\Entity\SearchQuery;
use AppBundle\Entity\ConceptLabel;
use AppBundle\Form\SearchQueryType;

class LangualInfoController extends Controller {

    /**
     * @Route("/langual.html", name="show_langual_info")
     */
    public function ShowLangualInfoAction(Request $request) {
        $csv = "<html><head><meta charset=\"UTF-8\" /></head><body><table>";

        $repository = $this->getDoctrine()->getRepository(Term::class);
        $entities = $repository->findAll();

        $maxcodes1 = 0;
        $maxcodes2 = 0;
        foreach ($entities as $e) {
            $cc1 = 0;
            $cc2 = 0;
            foreach ($e->getSkosNotes() as $l) {
                if ($l->getNoteValue() && $l->getNoteType() && $l->getNoteType()->getValue() == "Langual code") {
                    $cc1++;
                }
                if ($l->getNoteValue() && $l->getNoteType() && $l->getNoteType()->getValue() == "Σχόλιο στον Langual") {
                    $cc2++;
                }
            }
            if ($cc1 > $maxcodes1)
                $maxcodes1 = $cc1;
            if ($cc2 > $maxcodes2)
                $maxcodes2 = $cc2;
        }


        $csv .= "<tr><td>ID</td><td>Concept</td>";
        for ($i = 0; $i < $maxcodes1; $i++)
            $csv .= "<td>Langual code " . ($i + 1) . "</td>";
        for ($i = 0; $i < $maxcodes2; $i++)
            $csv .= "<td>Langual Comment " . ($i + 1) . "</td>";
        $csv .= "</tr>";

        foreach ($entities as $e) {
            $csv .= "<tr><td><a href=\"http://gretaste.ilsp.gr/admin/?action=edit&entity=Concept&id=" . $e->getId() . "\">" . $e->getId() . "</a></td>"
                    . "<td>" . htmlspecialchars($e->getHead()) . "</td>";
            $cc1 = 0;
            $cc2 = 0;
            foreach ($e->getSkosNotes() as $l) {
                if ($l->getNoteValue() && $l->getNoteType() && $l->getNoteType()->getValue() == "Langual code") {
                    $csv .= "<td>";
                    $csv .= htmlspecialchars($l->getNoteValue());
                    $csv .= "</td>";
                    $cc1++;
                }
            }
            for ($i = $cc1; $i < $maxcodes1; $i++)
                $csv .= "<td></td>";
            foreach ($e->getSkosNotes() as $l) {
                if ($l->getNoteValue() && $l->getNoteType() && $l->getNoteType()->getValue() == "Σχόλιο στον Langual") {
                    $csv .= "<td>";
                    $csv .= htmlspecialchars($l->getNoteValue());
                    $csv .= "</td>";
                    $cc2++;
                }
            }
            for ($i = $cc1; $i < $maxcodes1 - 1; $i++)
                $csv .= "<td></td>";
            $csv .= "</tr>";
        }
        $csv .= "</table></body></html>";
        $response = new Response($csv);
        //$response->headers->set('Content-Type', 'text/csv');
        //$response->headers->set('Content-Disposition', 'attachment; filename="langual.csv"');
        return $response;
    }

}
