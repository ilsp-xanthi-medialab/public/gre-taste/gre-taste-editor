<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\ConceptLabel;
use AppBundle\Entity\SearchQuery;
use AppBundle\Entity\SkosLabelType;
use AppBundle\Entity\SkosLanguage;
use AppBundle\Entity\SkosNoteType;
use AppBundle\Entity\SkosNote;
use AppBundle\Entity\SkosRelationType;
use AppBundle\Entity\SkosRelation;
use AppBundle\Entity\Term;
use AppBundle\Form\SearchQueryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TreeController extends Controller {

    /**
     * @Route("/admin/concepts/tree", name="concepts_tree")
     */
    public function treeAction(Request $request) {
        $id = $request->query->get('id');
        return $this->render('default/tree.html.twig', ['visit' => $id]);
    }

    /**
     * @Route("/concepts/tree/roots.json", name="concepts_tree_roots")
     */
    public function treeRootsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concepts = $repository->findBy(
                ['parent' => null], ['head' => 'ASC']
        );
        $response = new JsonResponse();
        $result = array();
        foreach ($concepts as $c) {
            $text = $c->getHead();
            if ($c->getIsFacet())
                $text = '' . $c->getHead() . '';
            $result[] = array("id" => $c->getId(), "children" => !$c->getChildren()->isEmpty(), "text" => $text, "type" => $c->getIsFacet() ? "facet" : "default");
        }


        usort($result, function($a, $b) {
            $x = $a['text'];
            $y = $b['text'];

            $x = mb_convert_case($x, MB_CASE_UPPER, "UTF-8");
            $y = mb_convert_case($y, MB_CASE_UPPER, "UTF-8");

            $x = str_replace(
                    ["ά", "έ", "ί", "ύ", "ή", "ό", "ώ", "Ά", "Έ", "Ί", "Ή", "Ύ", "Ό", "Ώ", "ϋ", "ϊ", "ΰ", "ΐ", "Ϋ", "Ϊ"],
                    ["α", "ε", "ι", "υ", "η", "ο", "ω", "Α", "Ε", "Ι", "Η", "Υ", "Ο", "Ω", "υ", "ι", "υ", "ι", "Υ", "Ι"],
                    $x);

            $y = str_replace(
                    ["ά", "έ", "ί", "ύ", "ή", "ό", "ώ", "Ά", "Έ", "Ί", "Ή", "Ύ", "Ό", "Ώ", "ϋ", "ϊ", "ΰ", "ΐ", "Ϋ", "Ϊ"],
                    ["α", "ε", "ι", "υ", "η", "ο", "ω", "Α", "Ε", "Ι", "Η", "Υ", "Ο", "Ω", "υ", "ι", "υ", "ι", "Υ", "Ι"],
                    $y);

            return strnatcasecmp($x, $y);
        });


        $response->setData($result);
        return $response;
    }

    /**
     * @Route("/concepts/tree/children.json", name="concepts_tree_children")
     */
    public function treeChildsAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concept = $repository->find($request->query->get('id'));
        $response = new JsonResponse();
        $result = array();
        foreach ($concept->getChildren() as $c) {
            $text = $c->getHead();
            if ($c->getIsFacet())
                $text = '' . $c->getHead() . '';
            //if($c->getParent() != null)              
            //$text .= ' ['.$c->getParent()->getHead().']';
            $result[] = array("id" => $c->getId(), "children" => !$c->getChildren()->isEmpty(), "text" => $text, "type" => $c->getIsFacet() ? "facet" : "default");
        }
        if (0)
            foreach ($concept->getSkosRelations() as $r) {
                if ($r->getRelationType() && $r->getRelationType()->getIsHierarchical() === true) {
                    $c = $r->getRelatedTerm();
                    $text = $c->getHead();
                    if ($c->getIsFacet())
                        $text = '' . $c->getHead() . '';
                    //if($c->getParent() != null)              
                    //$text .= ' ['.$c->getParent()->getHead().']';
                    $result[] = array("id" => $c->getId(), "children" => !$c->getChildren()->isEmpty(), "text" => $text, "type" => $c->getIsFacet() ? "facet" : "secondary");
                }
            }

        usort($result, function($a, $b) {
            $x = $a['text'];
            $y = $b['text'];

            $x = mb_convert_case($x, MB_CASE_UPPER, "UTF-8");
            $y = mb_convert_case($y, MB_CASE_UPPER, "UTF-8");

            $x = str_replace(
                    ["ά", "έ", "ί", "ύ", "ή", "ό", "ώ", "Ά", "Έ", "Ί", "Ή", "Ύ", "Ό", "Ώ", "ϋ", "ϊ", "ΰ", "ΐ", "Ϋ", "Ϊ"],
                    ["α", "ε", "ι", "υ", "η", "ο", "ω", "Α", "Ε", "Ι", "Η", "Υ", "Ο", "Ω", "υ", "ι", "υ", "ι", "Υ", "Ι"],
                    $x);

            $y = str_replace(
                    ["ά", "έ", "ί", "ύ", "ή", "ό", "ώ", "Ά", "Έ", "Ί", "Ή", "Ύ", "Ό", "Ώ", "ϋ", "ϊ", "ΰ", "ΐ", "Ϋ", "Ϊ"],
                    ["α", "ε", "ι", "υ", "η", "ο", "ω", "Α", "Ε", "Ι", "Η", "Υ", "Ο", "Ω", "υ", "ι", "υ", "ι", "Υ", "Ι"],
                    $y);

            return strnatcasecmp($x, $y);
        });
        $response->setData($result);
        return $response;
    }

    /**
     * @Route("/admin/tree/enableFacet", name="enableFacet")
     */
    public function enableFacetAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $value = $request->request->get('value');
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concept = $repository->find($value);
        $concept->setIsFacet(true);
        $entityManager->flush();
        $response = new JsonResponse();
        return $response;
    }

    /**
     * @Route("/admin/tree/disableFacet", name="disableFacet")
     */
    public function disableFacetAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        //$pk = $request->request->get('pk');
        //$name = $request->request->get('name');
        $value = $request->request->get('value');

        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concept = $repository->find($value);
        $concept->setIsFacet(false);
        $entityManager->flush();
        $response = new JsonResponse();
        return $response;
    }

    /**
     * @Route("/concept/parents2/{id}", name="get_concept_parents2")
     */
    public function getConceptParents2Action(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $response = new JsonResponse();
        $result = array();

        //$result[] = $id;
        //$id = $request->query->get('id');
        while (1) {

            $concept = $repository->find($id);
            if (!$concept->getParent())
                break;
            $id = $concept->getParent()->getId();
            $result[] = ['id' => $id, 'name' => $concept->getParent()->getHead()];
        }
        $result = array_reverse($result);

        $response->setData($result);
        return $response;
    }

    /**
     * @Route("/concept/parents/{id}", name="get_concept_parents")
     */
    public function getConceptParentsAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $response = new JsonResponse();
        $result = array();
        $concept = $repository->find($id);
        if ($concept == null) {
            $response->setData(array());
            return $response;
        }
        $result[] = $concept->getId();
        //$id = $request->query->get('id');
        while (1) {

            $concept = $repository->find($id);
            if (!$concept->getParent())
                break;
            $result[] = $id = $concept->getParent()->getId();
        }
        //$result = array_reverse($result);

        $response->setData($result);
        return $response;
    }

    /**
     * @Route("/concepts/view/{id}", name="viewConceptAction")
     */
    public function viewConceptAction(Request $request, $id) {
        $repository = $this->getDoctrine()->getRepository(Term::class);
        $concept = $repository->find($id);
        $repository2 = $this->getDoctrine()->getRepository(SkosRelation::class);
        $related = $repository2->findBy(['relatedTerm' => $id], array('relationType' => 'ASC', 'term' => 'ASC'));
        //$response = new JsonResponse();
        //$result = array();
        //$result[] = array("id" => $concept->getId(), "children" => !$concept->getChildren()->isEmpty(), "text" => $concept->getHead());
        //$response->setData($result);

        $visited = [];
        $inherited = $this->getParentRelations($concept, $visited);

        $allRelations = $this->getDoctrine()->getRepository(SkosRelationType::class)->findAll();
        $filteredRelations = [];
        foreach ($allRelations as $relation) {
            if ($this->hasDomain($concept, $relation)) {
                $filteredRelations[] = $relation;
            }
        }

        return $this->render('default/concept.html.twig', [
                    'e' => $concept,
                    'languages' => $this->getDoctrine()->getRepository(SkosLanguage::class)->findAll(),
                    'articles' => $this->getDoctrine()->getRepository(Article::class)->findAll(),
                    'rtypes' => $filteredRelations,
                    'ntypes' => $this->getDoctrine()->getRepository(SkosNoteType::class)->findAll(),
                    'ttypes' => $this->getDoctrine()->getRepository(SkosLabelType::class)->findAll(),
                    'related' => $related,
                    'inherited' => $inherited
        ]);
        //return $response;
    }

    public function getParentRelations($concept, &$visited) {

        if (!$concept)
            return [];

        if (in_array($concept->getId(), $visited))
            return [];
        $visited[] = $concept->getId();

        $cr = $concept->getSkosRelations()->toArray();
        $ar = [];
        $ar = array_merge($ar, $cr);
        $parent = $concept->getParent();
        if ($parent)
            $ar = array_merge($ar, $this->getParentRelations($parent, $visited));

        foreach ($cr as $r) {
            if ($r->getRelationType() && $r->getRelationType()->getIsHierarchical()) {
                $n = $this->getParentRelations($r->getRelatedTerm(), $visited);
                $ar = array_merge($ar, $n);
            }
        }


        return $ar;
    }

    /**
     * @Route("/admin/tree/edit/ajax", name="treeEditAJAXAction")
     */
    public function treeEditAJAXAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(ConceptLabel::class);
        $pk = $request->request->get('pk');
        $name = $request->request->get('name');
        $value = $request->request->get('value');

        if ($pk == 0) {

            $e = new ConceptLabel();
            $e->setTerm($this->getDoctrine()->getRepository(Term::class)->find($value));
            $entityManager->persist($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }
        if ($pk == -1) {
            $e = $repository->find($value);
            $e->setTerm(null);

            $entityManager->remove($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }

        $e = $repository->find($pk);
        if ($name === "noteValue") {
            $e->setNoteValue($value);
        }
        if ($name === "noteType") {
            $e->setNoteType($this->getDoctrine()->getRepository(SkosLabelType::class)->find($value));
        }
        if ($name === "article") {
            $e->setArticle($this->getDoctrine()->getRepository(Article::class)->find($value));
        }
        if ($name === "noteLanguage") {
            $e->setNoteLanguage($this->getDoctrine()->getRepository(SkosLanguage::class)->find($value));
        }

        $entityManager->flush();

        $response = new JsonResponse();
        return $response;
    }

    /**
     * @Route("/admin/tree/edit/ajax2", name="treeEditAJAXAction2")
     */
    public function treeEditAJAXAction2(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(SkosRelation::class);
        $pk = $request->request->get('pk');
        $name = $request->request->get('name');
        $value = $request->request->get('value');

        if ($pk == 0) {

            $e = new SkosRelation();
            $e->setTerm($this->getDoctrine()->getRepository(Term::class)->find($value));
            $entityManager->persist($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }if ($pk == -1) {
            $e = $repository->find($value);
            $e->setTerm(null);

            $entityManager->remove($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }


        $e = $repository->find($pk);
        if ($name === "noteValue") {
            $e->setNoteValue($value);
        }
        if ($name === "relationType") {
            $e->setRelationType($this->getDoctrine()->getRepository(SkosRelationType::class)->find($value));
        }
        if ($name === "relatedTerm") {
            $e->setRelatedTerm($this->getDoctrine()->getRepository(Term::class)->find($value));
        }

        $entityManager->flush();

        $response = new JsonResponse();
        return $response;
    }

    /**
     * @Route("/admin/tree/edit/ajax3", name="treeEditAJAXAction3")
     */
    public function treeEditAJAXAction3(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(SkosNote::class);
        $pk = $request->request->get('pk');
        $name = $request->request->get('name');
        $value = $request->request->get('value');


        if ($pk == 0) {

            $e = new SkosNote();
            $e->setTerm($this->getDoctrine()->getRepository(Term::class)->find($value));
            $entityManager->persist($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }if ($pk == -1) {
            $e = $repository->find($value);
            $e->setTerm(null);

            $entityManager->remove($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }



        $e = $repository->find($pk);
        if ($name === "noteValue") {
            $e->setNoteValue($value);
        }
        if ($name === "noteType") {
            $e->setNoteType($this->getDoctrine()->getRepository(SkosNoteType::class)->find($value));
        }
        if ($name === "noteLanguage") {
            $e->setNoteLanguage($this->getDoctrine()->getRepository(SkosLanguage::class)->find($value));
        }

        $entityManager->flush();

        $response = new JsonResponse();
        return $response;
    }

    //check range
    private function hasRange($concept, $relation) {
        if ($relation == null || $relation->getRelationType() == null) {
            return true;
        }
        if ($relation->getRelationType()->getRangeFacet1() == null && $relation->getRelationType()->getRangeFacet2() == null && $relation->getRelationType()->getRangeFacet3() == null) {
            return true;
        }
        $parent = $concept->getParent();
        while ($parent != null) {

            if ($relation->getRelationType()->getRangeFacet1() != null &&
                    $parent == $relation->getRelationType()->getRangeFacet1()) {
                return true;
            }
            if ($relation->getRelationType()->getRangeFacet2() != null &&
                    $parent == $relation->getRelationType()->getRangeFacet2()) {
                return true;
            }
            if ($relation->getRelationType()->getRangeFacet3() != null &&
                    $parent == $relation->getRelationType()->getRangeFacet3()) {
                return true;
            }
            $parent = $parent->getParent();
        }

        return false;
    }

    private function hasDomain($concept, $relationType) {
        if ($relationType==null) {
            return true;
        }
        if ($relationType->getDomainFacet1() == null && $relationType->getDomainFacet2() == null && $relationType->getDomainFacet3() == null) {
            return true;
        }
       
        $parent = $concept->getParent();
        while ($parent != null) {

            if ($relationType->getDomainFacet1() != null &&
                    $parent == $relationType->getDomainFacet1()) {
                return true;
            }
            if ($relationType->getDomainFacet2() != null &&
                    $parent == $relationType->getDomainFacet2()) {
                return true;
            }
            if ($relationType->getDomainFacet3() != null &&
                    $parent == $relationType->getDomainFacet3()) {
                return true;
            }
            $parent = $parent->getParent();
        }

        return false;
    }

    /**
     * @Route("/getConcepts", name="getCountries")
     */
    public function getCountries(Request $request) {
        $data = $request->get('query');
        $fil = $request->get('fil');

        $relation = $this->getDoctrine()->getRepository(SkosRelation::class)->find($fil);

        $e = $this->getDoctrine()->getRepository(Term::class)->createQueryBuilder('a')
                        ->where('a.head LIKE :head')
                        ->setParameter('head', '%' . $data . '%')
                        ->getQuery()->getResult();

        $data = [];
        foreach ($e as $o)
            if ($this->hasRange($o, $relation)) {
                //$CountryName = $o->getHead();
                //if($o->getIsFacet())
                //$CountryName = "<" . $CountryName . ">";
                //if($o->getParent() !=null)
                //$CountryName .= " [" . $o->getParent()->getHead() . "]";
                $data[] = ["Id" => $o->getId(), "Name" => $o->getFormattedName(), "Parent" => $o->getFormattedParents()];
            }
        $response = new JsonResponse($data);
        return $response;


        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(SkosNote::class);
        $pk = $request->request->get('pk');
        $name = $request->request->get('name');
        $value = $request->request->get('value');


        if ($pk == 0) {
            $e = new SkosNote();
            $e->setTerm($this->getDoctrine()->getRepository(Term::class)->find($value));
            $entityManager->persist($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }
        if ($pk == -1) {
            $e = $repository->find($value);
            $e->setTerm(null);

            $entityManager->remove($e);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }



        $e = $repository->find($pk);
        if ($name === "noteValue") {
            $e->setNoteValue($value);
        }
        if ($name === "noteType") {
            $e->setNoteType($this->getDoctrine()->getRepository(SkosNoteType::class)->find($value));
        }
        if ($name === "noteLanguage") {
            $e->setNoteLanguage($this->getDoctrine()->getRepository(SkosLanguage::class)->find($value));
        }

        $entityManager->flush();

        $response = new JsonResponse();
        return $response;
    }

    /**
     * @Route("/getCountryById/{cid}", name="getCountryById")
     */
    public function getCountryById(Request $request, $cid) {
        $e = $this->getDoctrine()->getRepository(Term::class)->find($cid);
        $response = new JsonResponse(["Id" => $e->getId(), "Name" => $e->getFormattedName(), "Parent" => $e->getFormattedParents()]);
        return $response;
    }

    /**
     * @Route("/admin/tree/ajax", name="treeAJAXAction")
     */
    public function treeAJAXAction(Request $request) {
        $content = $request->getContent();

        if (empty($content)) {
            throw new BadRequestHttpException("Content is empty");
        }

        //if(!Validator::isValidJsonString($content)){
        //throw new BadRequestHttpException("Content is not a valid json");
        //}


        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Term::class);
        $r = json_decode($content);

        if ($r->op == 'delete') {
            $c = $repository->find($r->id);
            if ($c == null) {
                throw new BadRequestHttpException("Content is empty");
            }
            foreach ($c->getChildren() as $child)
                $child->setParent(null);
            //$entityManager->flush();
            $entityManager->remove($c);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }
        if ($r->op == 'rename') {
            $c = $repository->find($r->id);
            if ($c == null) {
                throw new BadRequestHttpException("Content is empty");
            }
            $c->setHead($r->text);
            $entityManager->flush();

            $response = new JsonResponse();
            return $response;
        }
        if ($r->op == 'create') {
            $term = new Term();
            $p = $repository->find($r->parent);
            $term->setParent($p);
            #$term->set
            $term->setHead($r->text);
            $entityManager->persist($term);
            $entityManager->flush();

            $response = new JsonResponse($term->getId());
            return $response;
        }

        $c = $repository->find($r->id);
        $p = null;
        if ($r->parent === "#") {
            
        } else {
            $p = $repository->find($r->parent);
            if ($p == null) {
                throw new BadRequestHttpException("Content is empty");
            }
        }

        if ($c == null) {
            throw new BadRequestHttpException("Content is empty");
        }


        $c->setParent($p);
        $entityManager->flush();

        $response = new JsonResponse();
        return $response;
    }

}
