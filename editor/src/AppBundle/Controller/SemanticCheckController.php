<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Term;
use AppBundle\Entity\SkosNoteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SemanticCheckController extends Controller {

    public function getParentRelations($concept, &$visited) {

        if (!$concept) {
            return [];
        }
        if (in_array($concept->getId(), $visited)) {
            return [];
        }
        $visited[] = $concept->getId();

        $cr = $concept->getSkosRelations()->toArray();
        $ar = array_merge([], $cr);
        $parent = $concept->getParent();
        if ($parent) {
            $ar = array_merge($ar, $this->getParentRelations($parent, $visited));
        }
        foreach ($cr as $r) {
            if ($r->getRelationType() && $r->getRelationType()->getIsHierarchical()) {
                $n = $this->getParentRelations($r->getRelatedTerm(), $visited);
                $ar = array_merge($ar, $n);
            }
        }


        return $ar;
    }

    /**
     * @Route("/dictionary.report.html", name="semantic_check")
     */
    public function semanticCheckAction(Request $request) {
        $repository = $this->getDoctrine()->getRepository(Term::class);

        $noteTypeRepository = $this->getDoctrine()->getRepository(SkosNoteType::class);

        $array = [];
        $entities = $repository->findAll();

        $relation_count = 0;
        $i_relation_count = 0;
        $i_c_count = 0;
        $i_cc_count = 0;
        //$cc=[];
        foreach ($entities as $concept) {
            if (count($concept->getChildren())) {
                $i_c_count++;
                $i_cc_count += count($concept->getChildren());
            }
            $visited = [];
            $inherited = $this->getParentRelations($concept, $visited);
            foreach ($inherited as $l) {
                $i_relation_count++;
            }
            $hasel = 0;
            $hasen = 0;
            $hasru = 0;
            foreach ($concept->getSkosLabels() as $l) {

                $s = '';
                if ($l->getNoteType() !== null) {
                    $s .= ' ';
                    $s .= $l->getNoteType()->getValue();
                }

                if ($l->getNoteValue() !== null) {
                    $s .= ' ';
                    $s .= $l->getNoteValue();
                } else {
                    $s .= '<missing>';
                }
                if ($l->getNoteLanguage() !== null) {
                    $s .= '@';
                    $s .= $l->getNoteLanguage()->getValue();
                }
                if ($l->getNoteType() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'term',
                        'd' => $s,
                        'e' => 'missing type'
                    ];
                }
                if ($l->getNoteValue() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'term',
                        'd' => $s,
                        'e' => 'missing translation'
                    ];
                }
                if ($l->getNoteLanguage() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'term',
                        'd' => $s,
                        'e' => 'missing language'
                    ];
                } else {
                    if ($l->getNoteLanguage()->getValue() === 'en') {
                        $hasen++;
                    }
                    if ($l->getNoteLanguage()->getValue() === 'el') {
                        $hasel++;
                    }
                    if ($l->getNoteLanguage()->getValue() === 'ru') {
                        $hasru++;
                    }
                }
            }

            if ($hasel == 0)
                $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                    'm' => 'term',
                    'd' => 'missing translation for language el',
                    'e' => 'missing translation for language'
                ];
            if ($hasen == 0)
                $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                    'm' => 'term',
                    'd' => 'missing translation for language en',
                    'e' => 'missing translation for language'
                ];
            if ($hasru == 0)
                $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                    'm' => 'term',
                    'd' => 'missing translation for language ru',
                    'e' => 'missing translation for language'
                ];

            $alt1 = 0;
            $alt2 = 0;
            $alt3 = 0;

            foreach ($concept->getSkosRelations() as $l) {

                $relation_count++;

                if ($l->getRelationType() !== null) {
                    if ($l->getRelationType()->getValue() === 'Εναλλακτικό συστατικό 1') {
                        $alt1++;
                    }
                    if ($l->getRelationType()->getValue() === 'Εναλλακτικό συστατικό 2') {
                        $alt2++;
                    }
                    if ($l->getRelationType()->getValue() === 'Εναλλακτικό συστατικό 3') {
                        $alt3++;
                    }
                }
                $s = '';
                if ($l->getRelationType() !== null) {
                    $s .= ' ';
                    $s .= $l->getRelationType()->getValue();
                }
                if ($l->getRelatedTerm() !== null) {
                    $s .= ' ';
                    $s .= $l->getRelatedTerm()->getHead();
                }
                if ($l->getRelationType() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'relation', 'd' => $s, 'e' => 'missing related term type'];
                }
                if ($l->getRelatedTerm() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'relation', 'd' => $s, 'e' => 'missing related term'];
                }
            }
            if ($alt1 === 1) {
                $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                    'm' => 'relation', 'd' => 'Εναλλακτικό συστατικό 1', 'e' => 'only one value'];
            }
            if ($alt2 === 1) {
                $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                    'm' => 'relation', 'd' => 'Εναλλακτικό συστατικό 2', 'e' => 'only one value'];
            }
            if ($alt3 === 1) {
                $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                    'm' => 'relation', 'd' => 'Εναλλακτικό συστατικό 3', 'e' => 'only one value'];
            }

            $has_note = [];



            foreach ($concept->getSkosNotes() as $l) {

                $s = '';
                if ($l->getNoteType() !== null) {
                    if ($l->getNoteType()->getValue() === 'Changelog')
                        continue;
                    $s .= ' ';
                    $s .= $l->getNoteType()->getValue();
                }

                if ($l->getNoteValue() !== null) {
                    $s .= ' ';
                    $s .= mb_strimwidth($l->getNoteValue(), 0, 30, "...");
                } else {
                    $s .= '<missing>';
                }
                if ($l->getNoteType() !== null && $l->getNoteLanguage() !== null) {
                    if (!array_key_exists($l->getNoteLanguage()->getValue(), $has_note)) {
                        $has_note[$l->getNoteLanguage()->getValue()] = [];
                    }
                    $has_note[$l->getNoteLanguage()->getValue()][] = $l->getNoteType()->getId();
                }

                if ($l->getNoteLanguage() !== null) {
                    $s .= '@';
                    $s .= $l->getNoteLanguage();
                }

                if ($l->getNoteValue() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'note',
                        'd' => $s,
                        'e' => 'missing text'
                    ];
                }
                if ($l->getNoteType() === null) {
                    $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                        'm' => 'note',
                        'd' => $s,
                        'e' => 'missing type'
                    ];
                }
                if ($l->getNoteLanguage() === null) {
                    if ($l->getNoteType() !== null && $l->getNoteType()->getValue() == 'Langual code') {
                        
                    } else if ($l->getNoteType() !== null && $l->getNoteType()->getValue() == 'Σχόλιο στον Langual') {
                        
                    } else {
                        $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                            'm' => 'note',
                            'd' => $s,
                            'e' => 'missing language'];
                    }
                }
            }

            $note_ids = [1, 4, 5, 6, 7, 24];

            if (array_key_exists('el', $has_note) && array_key_exists('en', $has_note)) {
                foreach ($note_ids as $note_id) {
                    if (in_array($note_id, $has_note['el']) && !in_array($note_id, $has_note['en'])) {
                        $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                            'm' => 'note',
                            'd' => $noteTypeRepository->find($note_id)->getValue(),
                            'e' => 'missing en note'];
                    }
                }
            }
            if (array_key_exists('el', $has_note) && array_key_exists('ru', $has_note)) {
                foreach ($note_ids as $note_id) {
                    
                    if (in_array($note_id, $has_note['el']) && !in_array($note_id, $has_note['ru'])) {
                        $array[] = ['id' => $concept->getId(), 't' => $concept->gethead(),
                            'm' => 'note',
                            'd' => $noteTypeRepository->find($note_id)->getValue(),
                            'e' => 'missing ru note'];
                    }
                }
            }
        }


        $csv = "<html><head><meta charset=\"UTF-8\" />" . "<style>"
                . "table, th, td {"
                . "  border: 1px solid black;"
                . "  border-collapse: collapse;"
                . "}"
                . "th, td { padding: 2px 8px 2px 8px; }"
                . "</style>" . "</head><body><table>";




        $csv .= "<tr><td>#</td><td>Concept</td><td>Section</td><td>Details</td><td>Error</td></tr>";
        $i = 1;
        foreach ($array as $e) {
            $csv .= "<tr>"
                    . "<td>" . $i++ . "</td>"
                    . "<td><a href=\"/admin/concepts/tree?id=" . $e['id'] . "\">"
                    . htmlspecialchars($e['t']) . ' (#' . $e['id'] . ")</a></td>"
                    . "<td>" . htmlspecialchars($e['m']) . "</td>"
                    . "<td>" . htmlspecialchars($e['d']) . "</td>"
                    . "<td>" . htmlspecialchars($e['e']) . "</td>"
            ;


            $csv .= "</tr>";
        }
        $csv .= "</table>";

        $csv .= sprintf("<p>P=%d, H=%d, RR=%f, AVERAGE SUBCLASSES PER CLASSES=%f</p>", $relation_count, $i_relation_count,
                $relation_count / ($relation_count + $i_relation_count),
                $i_cc_count / $i_c_count
        );

        $csv .= "</body></html>";
        $response = new Response($csv);
        //dump($relation_count);
        //dump($i_relation_count);
        //dump($i_c_count);
        //dump($i_cc_count);
        return $response;
    }

}
