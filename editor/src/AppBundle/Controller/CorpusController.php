<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CorpusController extends Controller {

    /**
     * @Route("/admin/export/corpus.txt", name="corpus.txt")
     */
    public function indexAction(Request $request) {
        //try {
        $entityManager = $this->getDoctrine()->getManager();
        $dql = "SELECT d FROM AppBundle:CorpusDocument d";

        $fileSystem = new Filesystem();
        $file = $fileSystem->tempnam('/tmp', 'corpus_');
        $fp = fopen($file, 'w');
        if ($fp === false) {
            return new Response("can not create temp file");
        }

        $q = $entityManager->createQuery($dql);
        $iterableResult = $q->iterate();
        foreach ($iterableResult as $row) {
            $doc = $row[0];
            fwrite($fp, "origin: " . $doc->getOrigin() . "\n");
            if ($doc->getDescription())
                fwrite($fp, "description: " . $doc->getDescription() . "\n");
            fwrite($fp, "\n");
            if ($doc->getParagraphs()) {

                foreach ($doc->getParagraphs() as $p) {

                    //var_dump($p);

                    fwrite($fp, $p->getCategory() . "\n");
                    fwrite($fp, $p->getName() . "\n");
                    fwrite($fp, $p->getDescription() . "\n");
                    fwrite($fp, "\n");
                }
            }
            fwrite($fp, "\n");
            fwrite($fp, "\n");
            $entityManager->detach($row[0]);
            $entityManager->clear();
        }
        fclose($fp);
        $file = new File($file);
        return $this->file($file, 'corpus.txt');

        //} catch(Exception $e) {
        //return new Response( $e->getMessage());
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
        //}
    }

    /**
     * @Route("/admin/export/corpus2.txt", name="corpus2.txt")
     */
    public function index2Action(Request $request) {
        //try {
        $entityManager = $this->getDoctrine()->getManager();
        $dql = "SELECT d FROM AppBundle:CorpusDocument d";

        $fileSystem = new Filesystem();
        $file = $fileSystem->tempnam('/tmp', 'corpus_');
        $fp = fopen($file, 'w');
        if ($fp === false) {
            return new Response("can not create temp file");
        }

        $q = $entityManager->createQuery($dql);
        $iterableResult = $q->iterate();
        foreach ($iterableResult as $row) {
            $doc = $row[0];
            //fwrite($fp,  "origin: ".$doc->getOrigin()."\n");
            //if($doc->getDescription())
            //fwrite($fp,  "description: ".$doc->getDescription()."\n");
            //fwrite($fp,  "\n");
            if ($doc->getParagraphs()) {

                foreach ($doc->getParagraphs() as $p) {

                    //var_dump($p);
                    //fwrite($fp,  $p->getCategory()."\n");
                    fwrite($fp, $p->getName() . "\n");
                    fwrite($fp, $p->getDescription() . "\n");
                    fwrite($fp, "\n");
                }
            }
            //fwrite($fp,  "\n");
            //fwrite($fp,  "\n");
            $entityManager->detach($row[0]);
            $entityManager->clear();
        }
        fclose($fp);
        $file = new File($file);
        return $this->file($file, 'corpus.txt');

        //} catch(Exception $e) {
        //return new Response( $e->getMessage());
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
        //}
    }

    /**
     * @Route("/rest/corpus.json", name="corpus.json")
     */
    public function corpus_jsonAction(Request $request) {

        $entityManager = $this->getDoctrine()->getManager();
        $dql = "SELECT d FROM AppBundle:CorpusDocument d";

        $fileSystem = new Filesystem();
        $file = $fileSystem->tempnam('/tmp', 'corpus_json_');
        $fp = fopen($file, 'w');
        if ($fp === false) {
            return new Response("can not create temp file");
        }

        $q = $entityManager->createQuery($dql);
        $iterableResult = $q->iterate();

        $a = [];
        foreach ($iterableResult as $row) {
            $ol = new stdClass;
            $ol->c = "";
            if ($row[0]->getOrigin() != null) {
                $ol->origin = $row[0]->getOrigin()->getValue();
                $ol->c .= $row[0]->getOrigin()->getValue() . "\n";
            }
            if ('efood.gr' === $ol->origin) {
                continue;
            }
            if ($row[0]->getDescription()) {
                $ol->c .= $row[0]->getDescription() . "\n";
            }
            //$doc = $row[0];
            //$ol->p = [];


            if ($row[0]->getParagraphs()) {
                $last = null;
                foreach ($row[0]->getParagraphs() as $p) {
                    $pa = new stdClass;
                    //$ol->p[] = $pa;

                    if ($p->getCategory() && $p->getCategory() !== $last) {
                        $last = $pa->c = $p->getCategory();
                    }
                    if ($p->getName()){
                    //$pa->n 
                        $ol->c .= $p->getName() . "\n";
                    }
                    if ($p->getDescription()){
                    //$pa->d = 
                        $ol->c .= $p->getDescription() . "\n";
                    }

                    //fwrite($fp, $p->getName() . "\n");
                    //fwrite($fp, $p->getDescription() . "\n");
                }
            }
            $a[] = $ol;
            $entityManager->detach($row[0]);
            $entityManager->clear();
        }
        fwrite($fp, json_encode($a));
        fclose($fp);
        $file = new File($file);
        return $this->file($file, 'corpus.json');
    }

}
