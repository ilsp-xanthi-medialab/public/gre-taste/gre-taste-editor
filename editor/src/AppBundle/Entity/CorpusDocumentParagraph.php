<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * CorpusDocumentParagraph
 * 
 * @ORM\Table(name="corpus_document_paragraph")
 * @ORM\Entity
 */
class CorpusDocumentParagraph {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position = 0;

    /**
     * @var \AppBundle\Entity\CorpusDocument
     * 
     * @ORM\ManyToOne(targetEntity="CorpusDocument", inversedBy="paragraphs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     * })
     */
    private $document;

    /**
     * @var string
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(name="category", type="text", length=65535, nullable=true)
     */
    private $category;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="concept_id", referencedColumnName="id", nullable=true)
     */
    private $concept;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CorpusDocumentParagraph
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return CorpusDocumentParagraph
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CorpusDocumentParagraph
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set document
     *
     * @param \AppBundle\Entity\CorpusDocument $document
     *
     * @return CorpusDocumentParagraph
     */
    public function setDocument(\AppBundle\Entity\CorpusDocument $document = null) {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \AppBundle\Entity\CorpusDocument
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CorpusDocumentParagraph
     */
    public function setPosition($position) {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition() {
        return $this->position;
    }


    /**
     * Set concept.
     *
     * @param \AppBundle\Entity\Term|null $concept
     *
     * @return CorpusDocumentParagraph
     */
    public function setConcept(\AppBundle\Entity\Term $concept = null)
    {
        $this->concept = $concept;

        return $this;
    }

    /**
     * Get concept.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getConcept()
    {
        return $this->concept;
    }
}
