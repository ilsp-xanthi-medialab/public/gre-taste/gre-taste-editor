<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * CorpusDocument
 * 
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="corpus_document")
 * @ORM\Entity
 */
class CorpusDocument {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function __toString() {
        return '#'.$this->id;
    }
    /**
     * @ORM\ManyToOne(targetEntity="CorpusDocumentType")
     */
    private $type;
    /**
     * @ORM\ManyToOne(targetEntity="CorpusDocumentOrigin")
     */
    private $origin;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="modification_date", type="datetime", nullable=true)
     */
    private $modificationDate;

    /**
     *
     * @ORM\ManyToMany(targetEntity="CorpusDocumentSpatial")
     * @ORM\JoinTable(name="corpus_document_has_spatial",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="spatial_id", referencedColumnName="id")}
     *      )
     */
    private $spatial;
    /**
     *
     * @ORM\ManyToMany(targetEntity="CorpusInsertType")
     * @ORM\JoinTable(name="corpus_document_has_insert_method",
     *      joinColumns={@ORM\JoinColumn(name="document_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="insert_id", referencedColumnName="id")}
     *      )
     */
    private $insertMethods;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="CorpusDocumentParagraph", mappedBy="document", cascade={"persist"}, orphanRemoval=true)
     */
    private $paragraphs;
    
    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=250, nullable=true)
     */
    private $address;
    
    /**
     * @var string
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;    
    
    /**
     * @var string
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;        
    
    /**
     * @var string
     * 
     * @ORM\Column(name="is_valid", type="boolean", nullable=false)
     */
    private $isValid = true;
    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist() {
        $this->creationDate = new \DateTime("now");
        $this->modificationDate = new \DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate() {
        $this->modificationDate = new \DateTime("now");
    }




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->spatial = new \Doctrine\Common\Collections\ArrayCollection();
        $this->insertMethods = new \Doctrine\Common\Collections\ArrayCollection();
        $this->paragraphs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return CorpusDocument
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     *
     * @return CorpusDocument
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\CorpusDocumentType $type
     *
     * @return CorpusDocument
     */
    public function setType(\AppBundle\Entity\CorpusDocumentType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\CorpusDocumentType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set origin
     *
     * @param \AppBundle\Entity\CorpusDocumentOrigin $origin
     *
     * @return CorpusDocument
     */
    public function setOrigin(\AppBundle\Entity\CorpusDocumentOrigin $origin = null)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return \AppBundle\Entity\CorpusDocumentOrigin
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Add spatial
     *
     * @param \AppBundle\Entity\CorpusDocumentSpatial $spatial
     *
     * @return CorpusDocument
     */
    public function addSpatial(\AppBundle\Entity\CorpusDocumentSpatial $spatial)
    {
        $this->spatial[] = $spatial;

        return $this;
    }

    /**
     * Remove spatial
     *
     * @param \AppBundle\Entity\CorpusDocumentSpatial $spatial
     */
    public function removeSpatial(\AppBundle\Entity\CorpusDocumentSpatial $spatial)
    {
        $this->spatial->removeElement($spatial);
    }

    /**
     * Get spatial
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpatial()
    {
        return $this->spatial;
    }

    /**
     * Add insertMethod
     *
     * @param \AppBundle\Entity\CorpusInsertType $insertMethod
     *
     * @return CorpusDocument
     */
    public function addInsertMethod(\AppBundle\Entity\CorpusInsertType $insertMethod)
    {
        $this->insertMethods[] = $insertMethod;

        return $this;
    }

    /**
     * Remove insertMethod
     *
     * @param \AppBundle\Entity\CorpusInsertType $insertMethod
     */
    public function removeInsertMethod(\AppBundle\Entity\CorpusInsertType $insertMethod)
    {
        $this->insertMethods->removeElement($insertMethod);
    }

    /**
     * Get insertMethods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInsertMethods()
    {
        return $this->insertMethods;
    }

    /**
     * Add paragraph
     *
     * @param \AppBundle\Entity\CorpusDocumentParagraph $paragraph
     *
     * @return CorpusDocument
     */
    public function addParagraph(\AppBundle\Entity\CorpusDocumentParagraph $paragraph)
    {
        $this->paragraphs[] = $paragraph;
		$paragraph->setDocument($this);
        return $this;
    }

    /**
     * Remove paragraph
     *
     * @param \AppBundle\Entity\CorpusDocumentParagraph $paragraph
     */
    public function removeParagraph(\AppBundle\Entity\CorpusDocumentParagraph $paragraph)
    {
        $this->paragraphs->removeElement($paragraph);
        $paragraph->setDocument(null);
    }

    /**
     * Get paragraphs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParagraphs()
    {
        return $this->paragraphs;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CorpusDocumentParagraph
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return CorpusDocument
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }


    /**
     * Set latitude.
     *
     * @param string|null $latitude
     *
     * @return CorpusDocument
     */
    public function setLatitude($latitude = null)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude.
     *
     * @return string|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude.
     *
     * @param string|null $longitude
     *
     * @return CorpusDocument
     */
    public function setLongitude($longitude = null)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude.
     *
     * @return string|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set isValid.
     *
     * @param bool $isValid
     *
     * @return CorpusDocument
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid.
     *
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }
}
