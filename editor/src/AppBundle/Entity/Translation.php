<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * translation
 *
 * @ORM\Table(name="translation")
 * @ORM\Entity
 */
class Translation {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\SkosLanguage
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkosLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var string
     * 
     * @ORM\Column(name="greek", type="string", length=250, nullable=true)
     */
    private $greek;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="translation", type="string", length=250, nullable=true)
     */
    private $translation;

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }



    /**
     * Set greek.
     *
     * @param string|null $greek
     *
     * @return Translation
     */
    public function setGreek($greek = null)
    {
        $this->greek = $greek;

        return $this;
    }

    /**
     * Get greek.
     *
     * @return string|null
     */
    public function getGreek()
    {
        return $this->greek;
    }

    /**
     * Set translation.
     *
     * @param string|null $translation
     *
     * @return Translation
     */
    public function setTranslation($translation = null)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation.
     *
     * @return string|null
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * Set language.
     *
     * @param \AppBundle\Entity\SkosLanguage|null $language
     *
     * @return Translation
     */
    public function setLanguage(\AppBundle\Entity\SkosLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return \AppBundle\Entity\SkosLanguage|null
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
