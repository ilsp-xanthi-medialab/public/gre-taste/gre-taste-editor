<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Note
 *
 * @ORM\Table(name="skos_relation")
 * @ORM\Entity
 */
class SkosRelation {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Term
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Term", inversedBy="skosRelations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="term_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $term;

    /**
     * @var \AppBundle\Entity\SkosRelationType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkosRelationType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $relationType;



    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", length=65535, nullable=true)
     */
    private $noteURL;

    /**
     * @var \AppBundle\Entity\Term
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Term")
     *   @ORM\JoinColumn(name="related_term_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $relatedTerm;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set noteURL
     *
     * @param string $noteURL
     *
     * @return SkosNote
     */
    public function setNoteURL($noteURL)
    {
        $this->noteURL = $noteURL;

        return $this;
    }

    /**
     * Get noteURL
     *
     * @return string
     */
    public function getNoteURL()
    {
        return $this->noteURL;
    }

    /**
     * Set term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return SkosRelation
     */
    public function setTerm(\AppBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \AppBundle\Entity\Term
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set relationType
     *
     * @param \AppBundle\Entity\SkosRelationType $relationType
     *
     * @return SkosRelation
     */
    public function setRelationType(\AppBundle\Entity\SkosRelationType $relationType = null)
    {
        $this->relationType = $relationType;

        return $this;
    }

    /**
     * Get relationType
     *
     * @return \AppBundle\Entity\SkosRelationType
     */
    public function getRelationType()
    {
        return $this->relationType;
    }
    /**
     * Set term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return SkosRelation
     */
    public function setRelatedTerm(\AppBundle\Entity\Term $term = null)
    {
        $this->relatedTerm = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \AppBundle\Entity\Term
     */
    public function getRelatedTerm()
    {
        return $this->relatedTerm;
    }

}
