<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 * @ORM\Table(name="skos_relation_type")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SkosRelationType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var datetime $modified
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="value", type="string", length=250, nullable=false)
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(name="map_to", type="string", length=250, nullable=true)
     */
    private $mapTo;

    /**
     * @var string
     * 
     * @ORM\Column(name="description", type="text", length=65000, nullable=true)
     */
    private $description;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="range_facet1_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $rangeFacet1;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="range_facet2_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $rangeFacet2;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="range_facet3_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $rangeFacet3;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="domain_facet1_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $domainFacet1;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="domain_facet2_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $domainFacet2;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term")
     * @ORM\JoinColumn(name="domain_facet3_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $domainFacet3;

    /**
     * @var integer
     *
     * @ORM\Column(name="category", type="integer", options={"default" : 1})
     */
    private $category = 1;

    /**
     * Constructor
     */
    public function __construct() {
        $this->setCreated(new \DateTime());
        if ($this->getModified() == null) {
            $this->setModified(new \DateTime());
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime() {
        // update the modified time
        $this->setModified(new \DateTime());
        if ($this->getCreated() === null) {
            $this->setCreated($this->getModified());
        }
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return SkosRelationType
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName() {
        if (1 === $this->category) {
            return "Constituency and Function of the food";
        }
        if (2 === $this->category) {
            return "Nutritional and Dietary information";
        }
        if (3 === $this->category) {
            return "Cultural information";
        }
        if (4 === $this->category) {
            return "Dish structure";
        }
        return "?";
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory() {
        return $this->category;
    }

    public function __toString() {
        return $this->value;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return SkosRelationType
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SkosRelationType
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SkosRelationType
     */
    public function setMapTo($mapTo) {
        $this->mapTo = $mapTo;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getMapTo() {
        return $this->mapTo;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Term $parent
     *
     * @return SkosRelationType
     */
    public function setParent(\AppBundle\Entity\Term $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Term
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @var string
     * 
     * @ORM\Column(name="is_hierarchical", type="boolean", nullable=false)
     */
    private $isHierarchical = false;

    /**
     * Set isHierarchical
     *
     * @param boolean $isHierarchical
     *
     * @return SkosRelationType
     */
    public function setIsHierarchical($isHierarchical) {
        $this->isHierarchical = $isHierarchical;

        return $this;
    }

    /**
     * Get isHierarchical
     *
     * @return boolean
     */
    public function getIsHierarchical() {
        return $this->isHierarchical;
    }

    /**
     * @var string
     * 
     * @ORM\Column(name="is_bidirectional", type="boolean", nullable=false)
     */
    private $isBidirectional = false;

    /**
     * Set isBidirectional
     *
     * @param boolean $isBidirectional
     *
     * @return SkosRelationType
     */
    public function setIsBidirectional($isBidirectional) {
        $this->isBidirectional = $isBidirectional;

        return $this;
    }

    /**
     * Get isBidirectional
     *
     * @return boolean
     */
    public function getIsBidirectional() {
        return $this->isBidirectional;
    }

    /**
     * @var string
     * 
     * @ORM\Column(name="is_transitive", type="boolean", nullable=false)
     */
    private $isTransitive = false;

    /**
     * Set isTransitive
     *
     * @param boolean $isTransitive
     *
     * @return SkosRelationType
     */
    public function setIsTransitive($isTransitive) {
        $this->isTransitive = $isTransitive;

        return $this;
    }

    /**
     * Get isTransitive
     *
     * @return boolean
     */
    public function getIsTransitive() {
        return $this->isTransitive;
    }

    /**
     * @var string
     * 
     * @ORM\Column(name="is_symmetric", type="boolean", nullable=false)
     */
    private $isSymmetric = false;

    /**
     * Set isSymmetric
     *
     * @param boolean $isSymmetric
     *
     * @return SkosRelationType
     */
    public function setIsSymmetric($isSymmetric) {
        $this->isSymmetric = $isSymmetric;

        return $this;
    }

    /**
     * Get isSymmetric
     *
     * @return boolean
     */
    public function getIsSymmetric() {
        return $this->isSymmetric;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SkosRelationType
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return SkosRelationType
     */
    public function setModified($modified) {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified() {
        return $this->modified;
    }

    /**
     * Set rangeFacet2.
     *
     * @param \AppBundle\Entity\Term|null $rangeFacet2
     *
     * @return SkosRelationType
     */
    public function setRangeFacet2(\AppBundle\Entity\Term $rangeFacet2 = null) {
        $this->rangeFacet2 = $rangeFacet2;

        return $this;
    }

    /**
     * Get rangeFacet2.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getRangeFacet2() {
        return $this->rangeFacet2;
    }

    /**
     * Set rangeFacet3.
     *
     * @param \AppBundle\Entity\Term|null $rangeFacet3
     *
     * @return SkosRelationType
     */
    public function setRangeFacet3(\AppBundle\Entity\Term $rangeFacet3 = null) {
        $this->rangeFacet3 = $rangeFacet3;

        return $this;
    }

    /**
     * Get rangeFacet3.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getRangeFacet3() {
        return $this->rangeFacet3;
    }

    /**
     * Set domainFacet1.
     *
     * @param \AppBundle\Entity\Term|null $domainFacet1
     *
     * @return SkosRelationType
     */
    public function setDomainFacet1(\AppBundle\Entity\Term $domainFacet1 = null) {
        $this->domainFacet1 = $domainFacet1;

        return $this;
    }

    /**
     * Get domainFacet1.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getDomainFacet1() {
        return $this->domainFacet1;
    }

    /**
     * Set domainFacet2.
     *
     * @param \AppBundle\Entity\Term|null $domainFacet2
     *
     * @return SkosRelationType
     */
    public function setDomainFacet2(\AppBundle\Entity\Term $domainFacet2 = null) {
        $this->domainFacet2 = $domainFacet2;

        return $this;
    }

    /**
     * Get domainFacet2.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getDomainFacet2() {
        return $this->domainFacet2;
    }

    /**
     * Set domainFacet3.
     *
     * @param \AppBundle\Entity\Term|null $domainFacet3
     *
     * @return SkosRelationType
     */
    public function setDomainFacet3(\AppBundle\Entity\Term $domainFacet3 = null) {
        $this->domainFacet3 = $domainFacet3;

        return $this;
    }

    /**
     * Get domainFacet3.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getDomainFacet3() {
        return $this->domainFacet3;
    }


    /**
     * Set rangeFacet1.
     *
     * @param \AppBundle\Entity\Term|null $rangeFacet1
     *
     * @return SkosRelationType
     */
    public function setRangeFacet1(\AppBundle\Entity\Term $rangeFacet1 = null)
    {
        $this->rangeFacet1 = $rangeFacet1;

        return $this;
    }

    /**
     * Get rangeFacet1.
     *
     * @return \AppBundle\Entity\Term|null
     */
    public function getRangeFacet1()
    {
        return $this->rangeFacet1;
    }
}
