<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 * @ORM\Table(name="skos_note_type")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SkosNoteType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var datetime $modified
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="value", type="string", length=250, nullable=false)
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(name="map_to", type="string", length=250, nullable=true)
     */
    private $mapTo;

    /**
     * @var string
     * 
     * @ORM\Column(name="description", type="text", length=65000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="category", type="integer", options={"default" : 1})
     */
    private $category=1;

    /**
     * Constructor
     */
    public function __construct() {
        $this->setCreated(new \DateTime());
        if ($this->getModified() == null) {
            $this->setModified(new \DateTime());
        }
    }
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime() {
        // update the modified time
        $this->setModified(new \DateTime());
        if ($this->getCreated() === null) {
            $this->setCreated($this->getModified());
        }
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return SkosNoteType
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * Get categoryName
     *
     * @return string
     */
    public function getCategoryName()
    {
        if(1 === $this->category) {
		return "Constituency and Function of the food";
	}
        if(2 === $this->category) {
		return "Nutritional and Dietary information";
	}
        if(3 === $this->category) {
		return "Cultural information";
	}
        if(4 === $this->category) {
		return "Dish structure";
	}
	return "?";
    }

    public function __toString() {
        return $this->value;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return SkosNoteType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SkosNoteType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
        /**
     * Set description
     *
     * @param string $description
     *
     * @return SkosNoteType
     */
    public function setMapTo($mapTo)
    {
        $this->mapTo = $mapTo;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getMapTo()
    {
        return $this->mapTo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SkosNoteType
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return SkosNoteType
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }
}
