<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 * @ORM\Table(name="skos_label_has_source")
 * @ORM\Entity
 */
class LabelSource {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="url", type="text", length=4096, nullable=true)
     */
    private $url;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToOne(targetEntity="Source")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="source_type_id", referencedColumnName="id", nullable=true)
     * })
     * 
     */
    private $value;

    /**
     * @var \AppBundle\Entity\ConceptLabel
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ConceptLabel", inversedBy="termSources",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="concept_label_id", referencedColumnName="id")
     * })
     */
    private $label;

    public function __toString() {
        return "ID: " . $this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param \AppBundle\Entity\Source $value
     *
     * @return LabelSource
     */
    public function setValue(\AppBundle\Entity\Source $value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return \AppBundle\Entity\Source
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return LabelSource
     */
    public function setUrl($url) {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set value
     *
     * @param \AppBundle\Entity\ConceptLabel $value
     *
     * @return LabelSource
     */
    public function setLabel(\AppBundle\Entity\ConceptLabel $value) {
        $this->label = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return \AppBundle\Entity\ConceptLabel
     */
    public function getLabel() {
        return $this->label;
    }

}
