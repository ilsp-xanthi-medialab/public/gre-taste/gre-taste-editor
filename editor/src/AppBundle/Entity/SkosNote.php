<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Note
 *
 * @ORM\Table(name="skos_note")
 * @ORM\Entity
 */
class SkosNote {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Term
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Term", inversedBy="skosNotes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="term_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $term;

    /**
     * @var \AppBundle\Entity\SkosNoteType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkosNoteType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $noteType;

    /**
     * @var \AppBundle\Entity\SkosLanguage
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkosLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $noteLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=true)
     */
    private $noteValue;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", length=65535, nullable=true)
     */
    private $noteURL;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="skos_note_user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set noteValue
     *
     * @param string $noteValue
     *
     * @return SkosNote
     */
    public function setNoteValue($noteValue)
    {
        $this->noteValue = $noteValue;

        return $this;
    }

    /**
     * Get noteValue
     *
     * @return string
     */
    public function getNoteValue()
    {
        return $this->noteValue;
    }

    /**
     * Set noteURL
     *
     * @param string $noteURL
     *
     * @return SkosNote
     */
    public function setNoteURL($noteURL)
    {
        $this->noteURL = $noteURL;

        return $this;
    }

    /**
     * Get noteURL
     *
     * @return string
     */
    public function getNoteURL()
    {
        return $this->noteURL;
    }

    /**
     * Set term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return SkosNote
     */
    public function setTerm(\AppBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \AppBundle\Entity\Term
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set noteType
     *
     * @param \AppBundle\Entity\SkosNoteType $noteType
     *
     * @return SkosNote
     */
    public function setNoteType(\AppBundle\Entity\SkosNoteType $noteType = null)
    {
        $this->noteType = $noteType;

        return $this;
    }

    /**
     * Get noteType
     *
     * @return \AppBundle\Entity\SkosNoteType
     */
    public function getNoteType()
    {
        return $this->noteType;
    }

    /**
     * Set noteLanguage
     *
     * @param \AppBundle\Entity\SkosLanguage $noteLanguage
     *
     * @return SkosNote
     */
    public function setNoteLanguage(\AppBundle\Entity\SkosLanguage $noteLanguage = null)
    {
        $this->noteLanguage = $noteLanguage;

        return $this;
    }

    /**
     * Get noteLanguage
     *
     * @return \AppBundle\Entity\SkosLanguage
     */
    public function getNoteLanguage()
    {
        return $this->noteLanguage;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return SkosNote
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    
    public function __toString() {
	
	return $this->id . ' ' . $this->noteValue;
    }
}
