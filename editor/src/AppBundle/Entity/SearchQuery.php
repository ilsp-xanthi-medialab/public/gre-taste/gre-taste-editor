<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
class SearchQuery
{
	/**
     * @Assert\NotBlank()
     */
    protected $query;

    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query)
    {
        $this->query = $query;
    }


}
