<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Term
 * 
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="skos_concept")
 * @ORM\Entity //(repositoryClass="AppBundle\Repository\LemmaRepository")
 */
class Term {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="head", type="string", length=250, nullable=false)
     */
    private $head;


    /**
     * @ORM\ManyToOne(targetEntity="Article")
     */
    private $article;

    /**
     * @var string
     * 
     * @ORM\Column(name="is_facet", type="boolean", nullable=false)
     */
    private $isFacet=false;


    /**
     * @var string
     * 
     * @ORM\Column(name="transcript", type="string", length=250, nullable=true)
     */
    private $transcript;

    /**
     * @var string
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var string
     * @ORM\Column(name="internal_notes", type="text", length=65535, nullable=true)
     */
    private $internal_notes;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="modification_date", type="datetime", nullable=true)
     */
    private $modificationDate;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $creator;



    /**
     * @ORM\OneToMany(targetEntity="SkosNote", mappedBy="term", cascade={"persist"}, orphanRemoval=true)
     */
    private $skosNotes;

    /**
     * @ORM\OneToMany(targetEntity="ConceptLabel", mappedBy="term", cascade={"persist"}, orphanRemoval=true)
     */
    private $skosLabels;

    /**
     * @ORM\OneToMany(targetEntity="SkosRelation", mappedBy="term", cascade={"persist"}, orphanRemoval=true)
     */
    private $skosRelations;

    /**
     * Many Term have Many Source.
     * @ORM\ManyToMany(targetEntity="Source")
     * @ORM\JoinTable(name="scos_concept_has_source",
     *      joinColumns={@ORM\JoinColumn(name="term_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="source_id", referencedColumnName="id")}
     *      )
     */
    private $termSources;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->modificationDate = new \DateTime('now');
        }
    }

    public function getImageFile() {
        return $this->imageFile;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getImage() {
        return $this->image;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist() {
        $this->creationDate = new \DateTime("now");
        $this->modificationDate = new \DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate() {
        $this->modificationDate = new \DateTime("now");
    }

    /**
     * One Category has Many Categories.
     * @ORM\OneToMany(targetEntity="Term", mappedBy="parent")
     */
    private $children;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Term", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    // ...

    /**
     * Constructor
     */
    public function __construct() {
        $this->setCreationDate(new \DateTime());
        if ($this->getModificationDate() == null) {
            $this->setModificationDate(new \DateTime());
        }
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skosNotes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skosLabels = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skosRelations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->termSources = new \Doctrine\Common\Collections\ArrayCollection();

    }

   

    public function getFormattedName() {

	$name = $this->head;
        if ($name === null) {
            $name = "#" . $this->id;
        }
        if ($this->getIsFacet()){
                $name = "<" . $name . ">";
        }
	return $name;
    }

    public function getFormattedParents() {

	$name = "";
       
	if ($this->getParent() !== null) {
		$name .= " [";
		$p = $this->getParent();
		$path = [];
		while($p) {
			$path[] = $p->getFormattedName();
			$p = $p->parent;
		}
		$path = array_reverse($path);
		$prefix = "";
		foreach($path as $one) {
			$name .= $prefix;
			$name .= $one;
			$prefix =  " ➤ ";
		}
		$name .= "]";
	}
	return $name;
    }
    public function getPath() {

	$name = "";
       
	if ($this->getParent() !== null) {

		$p = $this->getParent();
		$path = [];
		while($p) {
			$path[] = $p->getFormattedName();
			$p = $p->parent;
		}
		$path = array_reverse($path);
		$prefix = "";
		foreach($path as $one) {
			$name .= $prefix;
			$name .= $one;
			$prefix =  " ➤ ";
		}
		$name .= " ➤ ";
	}
	$name .= $this->getFormattedName();
	return $name;
    }
    public function getParentPath() {

	$name = "";
       
	if ($this->getParent() !== null) {

		$p = $this->getParent();
		$path = [];
		while($p) {
			$path[] = $p->getFormattedName();
			$p = $p->parent;
		}
		$path = array_reverse($path);
		$prefix = "";
		foreach($path as $one) {
			$name .= $prefix;
			$name .= $one;
			$prefix =  " ➤ ";
		}
		$name .= " ➤ ";
	}

	return $name;
    }
    public function __toString() {

	$name = $this->getFormattedName();
       	$name .= $this->getFormattedParents();
	return $name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set head
     *
     * @param string $head
     *
     * @return Term
     */
    public function setHead($head) {
        $this->head = $head;

        return $this;
    }

    /**
     * Get head
     *
     * @return string
     */
    public function getHead() {
        return $this->head;
    }

    /**
     * Set transcript
     *
     * @param string $transcript
     *
     * @return Term
     */
    public function setTranscript($transcript) {
        $this->transcript = $transcript;

        return $this;
    }

    /**
     * Get transcript
     *
     * @return string
     */
    public function getTranscript() {
        return $this->transcript;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Term
     */
    public function setComment($comment) {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Term
     */
    public function setNotes($notes) {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes() {
        return $this->notes;
    }

    /**
     * Set internalNotes
     *
     * @param string $internalNotes
     *
     * @return Term
     */
    public function setInternalNotes($internalNotes) {
        $this->internal_notes = $internalNotes;

        return $this;
    }

    /**
     * Get internalNotes
     *
     * @return string
     */
    public function getInternalNotes() {
        return $this->internal_notes;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Term
     */
    public function setCreationDate($creationDate) {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate() {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     *
     * @return Term
     */
    public function setModificationDate($modificationDate) {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate() {
        return $this->modificationDate;
    }




    /**
     * Set article
     *
     * @param \AppBundle\Entity\Article $article
     *
     * @return Term
     */
    public function setArticle(\AppBundle\Entity\Article $article = null) {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \AppBundle\Entity\Article
     */
    public function getArticle() {
        return $this->article;
    }


    /**
     * Set creator
     *
     * @param \AppBundle\Entity\User $creator
     *
     * @return Term
     */
    public function setCreator(\AppBundle\Entity\User $creator = null) {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * Add skosNote
     *
     * @param \AppBundle\Entity\SkosNote $skosNote
     *
     * @return Term
     */
    public function addSkosNote(\AppBundle\Entity\SkosNote $skosNote) {
        $this->skosNotes[] = $skosNote;
        $skosNote->setTerm($this);
        return $this;
    }

    /**
     * Remove skosNote
     *
     * @param \AppBundle\Entity\SkosNote $skosNote
     */
    public function removeSkosNote(\AppBundle\Entity\SkosNote $skosNote) {
        $this->skosNotes->removeElement($skosNote);
        $skosNote->setTerm(null);
    }

    /**
     * Get skosNotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkosNotes() {
        return $this->skosNotes;
    }

    /**
     * Add termSource
     *
     * @param \AppBundle\Entity\Source $termSource
     *
     * @return Term
     */
    public function addTermSource(\AppBundle\Entity\Source $termSource) {
        $this->termSources[] = $termSource;

        return $this;
    }

    /**
     * Remove termSource
     *
     * @param \AppBundle\Entity\Source $termSource
     */
    public function removeTermSource(\AppBundle\Entity\Source $termSource) {
        $this->termSources->removeElement($termSource);
    }

    /**
     * Get termSources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermSources() {
        return $this->termSources;
    }

   

    /**
     * Add skosRelations
     *
     * @param \AppBundle\Entity\SkosRelation $skosNote
     *
     * @return Term
     */
    public function addSkosRelation(\AppBundle\Entity\SkosRelation $skosNote) {
        $this->skosRelations[] = $skosNote;
        $skosNote->setTerm($this);
        return $this;
    }

    /**
     * Remove skosRelations
     *
     * @param \AppBundle\Entity\SkosRelation $skosNote
     */
    public function removeSkosRelation(\AppBundle\Entity\SkosRelation $skosNote) {
        $this->skosRelations->removeElement($skosNote);
        $skosNote->setTerm(null);
    }

    /**
     * Get skosRelations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkosRelations() {
        return $this->skosRelations;
    }

    /**
     * Add skosLabels
     *
     * @param \AppBundle\Entity\ConceptLabel $skosLabel
     *
     * @return Term
     */
    public function addSkosLabel(\AppBundle\Entity\ConceptLabel $skosLabel) {
        $this->skosLabels[] = $skosLabel;
        $skosLabel->setTerm($this);
        return $this;
    }

    /**
     * Remove skosLabels
     *
     * @param \AppBundle\Entity\ConceptLabel $skosLabel
     */
    public function removeSkosLabel(\AppBundle\Entity\ConceptLabel $skosLabel) {
        $this->skosLabels->removeElement($skosLabel);
        $skosLabel->setTerm(null);
    }

    /**
     * Get skosLabels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkosLabels() {
        return $this->skosLabels;
    }

    public function setParent($parent) {
        $this->parent = $parent;
        return $this;
    }

    public function getParent() {
        return $this->parent;
    }

    public function setChildren($children) {
        $this->children = $children;
        return $this;
    }

    public function getChildren() {
        return $this->children;
    }

    /**
     * Set isFacet
     *
     * @param boolean $isFacet
     *
     * @return Term
     */
    public function setIsFacet($isFacet) {
        $this->isFacet = $isFacet;

        return $this;
    }

    /**
     * Get isFacet
     *
     * @return boolean
     */
    public function getIsFacet() {
        return $this->isFacet;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Term $child
     *
     * @return Term
     */
    public function addChild(\AppBundle\Entity\Term $child) {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Term $child
     */
    public function removeChild(\AppBundle\Entity\Term $child) {
        $this->children->removeElement($child);
    }

}
