<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 * @ORM\Table(name="skos_label_type")
 * @ORM\Entity
 */
class SkosLabelType {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="value", type="string", length=250, nullable=false)
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(name="map_to", type="string", length=250, nullable=true)
     */
    private $mapTo;

    /**
     * @var string
     * 
     * @ORM\Column(name="description", type="text", length=65000, nullable=true)
     */
    private $description;

    public function __toString() {
        return $this->value;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return SkosNoteType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SkosNoteType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
        /**
     * Set description
     *
     * @param string $description
     *
     * @return SkosNoteType
     */
    public function setMapTo($mapTo)
    {
        $this->mapTo = $mapTo;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getMapTo()
    {
        return $this->mapTo;
    }
}
