<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Note
 *
 * @ORM\Table(name="skos_label")
 * @ORM\Entity
 */
class ConceptLabel {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Term
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Term", inversedBy="skosLabels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="term_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $term;

    /**
     * @var \AppBundle\Entity\SkosLabelType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkosLabelType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $noteType;

    /**
     * @var \AppBundle\Entity\SkosLanguage
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SkosLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $noteLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=250, nullable=true)
     */
    private $noteValue;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", length=250, nullable=true)
     */
    private $noteURL;
    /**
     * @ORM\ManyToOne(targetEntity="Article")
     */
    private $article;
    /**
     * @var string
     * 
     * @ORM\Column(name="transcript", type="string", length=250, nullable=true)
     */
    private $transcript;

    
        /**
     * Many Users have Many Groups.
     * @ORM\OneToMany(targetEntity="LabelSource", mappedBy="label",cascade={"persist"})
     * @ ORM\JoinTable(name="concept_label_has_source",
     *      joinColumns={@ORM\JoinColumn(name="concept_label_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="source_id", referencedColumnName="id")}
     *      )
     */
    private $termSources;
    
        /**
     * Constructor
     */
    public function __construct() {

        $this->termSources = new \Doctrine\Common\Collections\ArrayCollection();
	}
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set noteValue
     *
     * @param string $noteValue
     *
     * @return ConceptLabel
     */
    public function setNoteValue($noteValue)
    {
        $this->noteValue = $noteValue;

        return $this;
    }

    /**
     * Get noteValue
     *
     * @return string
     */
    public function getNoteValue()
    {
        return $this->noteValue;
    }

    /**
     * Set noteURL
     *
     * @param string $noteURL
     *
     * @return ConceptLabel
     */
    public function setNoteURL($noteURL)
    {
        $this->noteURL = $noteURL;

        return $this;
    }

    /**
     * Get noteURL
     *
     * @return string
     */
    public function getNoteURL()
    {
        return $this->noteURL;
    }

    /**
     * Set term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return ConceptLabel
     */
    public function setTerm(\AppBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \AppBundle\Entity\Term
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set noteType
     *
     * @param \AppBundle\Entity\SkosLabelType $noteType
     *
     * @return ConceptLabel
     */
    public function setNoteType(\AppBundle\Entity\SkosLabelType $noteType = null)
    {
        $this->noteType = $noteType;

        return $this;
    }

    /**
     * Get noteType
     *
     * @return \AppBundle\Entity\SkosLabelType
     */
    public function getNoteType()
    {
        return $this->noteType;
    }

    /**
     * Set noteLanguage
     *
     * @param \AppBundle\Entity\SkosLanguage $noteLanguage
     *
     * @return ConceptLabel
     */
    public function setNoteLanguage(\AppBundle\Entity\SkosLanguage $noteLanguage = null)
    {
        $this->noteLanguage = $noteLanguage;

        return $this;
    }

    /**
     * Get noteLanguage
     *
     * @return \AppBundle\Entity\SkosLanguage
     */
    public function getNoteLanguage()
    {
        return $this->noteLanguage;
    }
    
    
    /**
     * Set transcript
     *
     * @param string $transcript
     *
     * @return ConceptLabel
     */
    public function setTranscript($transcript) {
        $this->transcript = $transcript;

        return $this;
    }

    /**
     * Get transcript
     *
     * @return string
     */
    public function getTranscript() {
        return $this->transcript;
    }
    
        /**
     * Set article
     *
     * @param \AppBundle\Entity\Article $article
     *
     * @return ConceptLabel
     */
    public function setArticle(\AppBundle\Entity\Article $article = null) {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \AppBundle\Entity\Article
     */
    public function getArticle() {
        return $this->article;
    }
    
    

    
        /**
     * Add termSource
     *
     * @param \AppBundle\Entity\LabelSource $termSource
     *
     * @return Term
     */
    public function addTermSource(\AppBundle\Entity\LabelSource $termSource) {
        $this->termSources[] = $termSource;
	$termSource->setLabel($this);
        return $this;
    }

    /**
     * Remove termSource
     *
     * @param \AppBundle\Entity\LabelSource $termSource
     */
    public function removeTermSource(\AppBundle\Entity\LabelSource $termSource) {
        $this->termSources->removeElement($termSource);
	$termSource->setLabel(null);
    }

    /**
     * Get termSources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermSources() {
        return $this->termSources;
    }

    public function __toString() {

	
	return $this->noteValue;
    }
}
