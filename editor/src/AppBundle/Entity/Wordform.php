<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Wordform
 *
 * @ORM\Table(name="wordform")
 * @ORM\Entity
 */
class Wordform {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="word", type="string", length=250, nullable=false)
     */
    private $word;

    /**
     * @var string
     * 
     * @ORM\Column(name="lemma", type="string", length=250, nullable=false)
     */
    private $lemma;

    /**
     * @var string
     * 
     * @ORM\Column(name="tag", type="string", length=250, nullable=true)
     */
    private $tag;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word.
     *
     * @param string $word
     *
     * @return Wordform
     */
    public function setWord($word)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word.
     *
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set lemma.
     *
     * @param string $lemma
     *
     * @return Wordform
     */
    public function setLemma($lemma)
    {
        $this->lemma = $lemma;

        return $this;
    }

    /**
     * Get lemma.
     *
     * @return string
     */
    public function getLemma()
    {
        return $this->lemma;
    }

    /**
     * Set tag.
     *
     * @param string|null $tag
     *
     * @return Wordform
     */
    public function setTag($tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag.
     *
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }
}
