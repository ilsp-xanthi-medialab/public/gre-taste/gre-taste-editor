<?php 

// src/AppBundle/EventListener/SearchIndexer.php
namespace AppBundle\EventListener;

use AppBundle\Entity\SkosNote;
// for Doctrine < 2.4: use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;



class SearchIndexer
{

private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // only act on some "Product" entity
        if (!$entity instanceof SkosNote) {
            return;
        }

        $entityManager = $args->getObjectManager();
        // ... do something with the Product
	if($entity->getUser() === null) {

		if (null === $token = $this->tokenStorage->getToken()) {
        		return;
		}
		if (!is_object($user = $token->getUser())) {
        		// e.g. anonymous authentication
        		return;
    		}

		$entity->setUser($user);
	}
    }
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // only act on some "Product" entity
        if (!$entity instanceof SkosNote) {
            return;
        }

        $entityManager = $args->getObjectManager();
        // ... do something with the Product
	if($entity->getUser() === null) {

		if (null === $token = $this->tokenStorage->getToken()) {
        		return;
		}
		if (!is_object($user = $token->getUser())) {
        		// e.g. anonymous authentication
        		return;
    		}

		$entity->setUser($user);
	}
    }
}
