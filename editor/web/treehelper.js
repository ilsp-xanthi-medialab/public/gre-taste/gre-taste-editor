tree_history = [];

function update_tree_path2(response) {

	var $newdiv1 = $( "<span></span>" );
	for(i=0; i < response.length ; i++) {
		$newdiv1.append( "<a class='visit-concept btn btn-xs' data-pk='" + response[i].id + "'><strong>"+response[i].name+"</strong></a>" );
		if(i < response.length-1)
		$newdiv1.append( " → ");
	}
	$( "#concept_path" ).empty();
	$( "#concept_path" ).append( $newdiv1 );

}

function update_tree_path() {

	$.ajax({
		url: '/concept/parents2/'+$('#lemma_head').data( "pk" ), //'{{ path('treeEditAJAXAction') }}',
		type: "POST",
		//data: { pk: 0, value: $('#newNote').data( "pk" ) },
		success: update_tree_path2,
		error: function (response) {
			console.log('ajax failed: 1');
		}
	});





}

function update_tree_history() {


	var $newdiv1 = $( "<span></span>" );
	for(i=tree_history.length-1; i > -1 ; i--) {
		if(i!=tree_history.length-1) $newdiv1.append( " ← ");
		$newdiv1.append( "<a class='visit-concept btn btn-xs' data-pk='" + tree_history[i].id + "'><strong>"+tree_history[i].name+"</strong></a>" );
	}
	$( "#concepts_tree_history" ).empty();
	$( "#concepts_tree_history" ).append( $newdiv1 );



}

$(document).ready(function () {
	$('#container').layout();
	$('#paddingWrapper').layout();
});

$(document).ready(function () {
	$(document.body).on('change', '#facet-checkbox', function() {

		pk = $(this).data( "pk" );
		success = function(data, textStatus, jqXHR) {
			//$('#concepts_tree').jstree(true).refresh_node($('#facet-checkbox').data( "pk" ));
			//$('#concepts_tree').jstree(true).redraw_node($('#facet-checkbox').data( "pk" ));
			$('#concepts_tree').jstree(true).set_type($('#facet-checkbox').data( "pk" ), 'facet');
		};
		success2 = function(data, textStatus, jqXHR) {
			//$('#concepts_tree').jstree(true).refresh_node($('#facet-checkbox').data( "pk" ));
			//$('#concepts_tree').jstree(true).redraw_node($('#facet-checkbox').data( "pk" ));
			$('#concepts_tree').jstree(true).set_type($('#facet-checkbox').data( "pk" ), 'default');
		};
		if(this.checked) {
			$.ajax({
			  type: "POST",
			  url: '/admin/tree/enableFacet', //'{{ path('enableFacet') }}',
			  //data: { pk: 0, value: {{ e.id }} },
			  data: { pk: 0, value: $(this).data( "pk" ) },
			  success: success,
			  //dataType: dataType
			});
		} else {
			$.ajax({
			  type: "POST",
			  url: '/admin/tree/disableFacet', //'{{ path('disableFacet') }}',
			  //data: { pk: 0, value: {{ e.id }} },
			  data: { pk: 0, value: $(this).data( "pk" ) },
			  success: success2,
			  //dataType: dataType
			});
		}
	});


	$(document.body).on('click', '#newTerm', function(e) {
		e.preventDefault();
		$.ajax({
			url: '/admin/tree/edit/ajax', //'{{ path('treeEditAJAXAction') }}',
			type: "POST",
			data: { pk: 0, value: $('#newTerm').data( "pk" ) },
			success: function (response) {
				$('#event_result').load('/concepts/view/' + $('#newTerm').data( "pk" ));
			},
			error: function (response) {
				console.log('ajax failed: 2');
			}
		});
	});
	$(document.body).on('click', '#newRelation', function(e) {
		e.preventDefault();
		$.ajax({
			url: '/admin/tree/edit/ajax2', //'{{ path('treeEditAJAXAction') }}',
			type: "POST",
			data: { pk: 0, value: $('#newRelation').data( "pk" ) },
			success: function (response) {
				$('#event_result').load('/concepts/view/' + $('#newRelation').data( "pk" ));
			},
			error: function (response) {
				console.log('ajax failed: 3');
			}
		});
	});
	$(document.body).on('click', '#newNote', function(e) {
		e.preventDefault();
		$.ajax({
			url: '/admin/tree/edit/ajax3', //'{{ path('treeEditAJAXAction') }}',
			type: "POST",
			data: { pk: 0, value: $('#newNote').data( "pk" ) },
			success: function (response) {
				$('#event_result').load('/concepts/view/' + $('#newNote').data( "pk" ));
			},
			error: function (response) {
				console.log('ajax failed: 4');
			}
		});
	});

	recuvidata = [];

	_select_tree_nodes = function() {
		//if(response.length == 0) return;
		//id = recuvidata.pop();

		$('#concepts_tree').jstree(true).select_node(recuvidata[0]);
		$('#concepts_tree').jstree(true).activate_node(recuvidata[0]);
		//if(recuvidata.length == 0) {
		//	$('#concepts_tree').jstree(true).select_node(id);
		//	$('#concepts_tree').jstree(true).activate_node(id);
		//}
		//$('#concepts_tree').jstree(true).get_node(id).focus();
	};
	_open_tree_nodes = function() {
		//if(response.length == 0) return;
		//id = recuvidata.pop();

		$('#concepts_tree').jstree(true).open_node(recuvidata.splice(1), _select_tree_nodes);
		//if(recuvidata.length == 0) {
		//	$('#concepts_tree').jstree(true).select_node(id);
		//	$('#concepts_tree').jstree(true).activate_node(id);
		//}
		//$('#concepts_tree').jstree(true).get_node(id).focus();
	};


	_load_tree_nodes = function(response) {
		if(response.length == 0) return;
		recuvidata = response;
		$('#concepts_tree').jstree(true).load_node(response, _open_tree_nodes);
		//$('#concepts_tree').jstree(true).get_node(id).focus();
	};

	$(document.body).on('click', '.visit-concept', function(e) {
		e.preventDefault();
		if($('#concepts_tree').jstree(true).get_node($(this).data( "pk" ))) {
			$('#concepts_tree').jstree(true).select_node($(this).data( "pk" ));
			$('#concepts_tree').jstree(true).activate_node($(this).data( "pk" ));
		} else {
			$.ajax({
				url: '/concept/parents/'+$(this).data( "pk" ), //'{{ path('treeEditAJAXAction') }}',
				type: "POST",
				//data: { pk: 0, value: $('#newNote').data( "pk" ) },
				success: _load_tree_nodes,
				error: function (response) {
					console.log('ajax failed: 5');
				}
			});
		}
	});

});


$(document).ready(function () {

    function check_callback(operation, node, parent, position, more) {
		if(operation === "copy_node") return false;
		if(operation === "delete_node")
			if (confirm('Are you sure you want to delete this Term and all children?')) {
				return true;
			} else {
				return false;
		}

		return true;
    }

    var tree = $('#concepts_tree');
    tree.jstree({
        "core": {
            //"themes": {
            //    "variant": "small"
            //},
            'check_callback': check_callback,
            "multiple" : false, // no multiselection
            'data': {
                'url': function(node) {
                    return node.id === '#' ? '/concepts/tree/roots.json' : '/concepts/tree/children.json';
                },
                'data': function(node) {
                    return {
                        'id': node.id
                    };
                }
            },
        },
        "checkbox": {
            "keep_selected_style": false
        },
	"types" : {
      		"default" : {
		        //"icon" : "glyphicon glyphicon-flash"
		},
		"facet" : {
			"icon" : "glyphicon glyphicon-folder-open"
		}
	},
        "plugins": ["contextmenu", "wholerow", "state", "dnd", "types"]
    });

    //https://www.phpflow.com/php/create-rename-delete-node-using-jstree-php-mysql/

    function activate_node(e, data) {
	if(typeof data.instance.get_node(data.node).id !== "undefined") {

		var n = {id: data.instance.get_node(data.node).id, name: data.instance.get_node(data.node).text };
		var index = tree_history.map(function(e) { return e.id; }).indexOf(n.id);
		if(index != -1) tree_history.splice(index,1);
		tree_history.push(n);
		if(tree_history.length > 40)
			tree_history.shift();
		update_tree_history();
	        $('#event_result').load('/concepts/view/' + data.instance.get_node(data.node).id);
	}
    }

    function create_node(e, data) {
        function create_node_callback(cb_data) {
            tree.jstree(true).set_id(data.node,cb_data);
	}
		
	var reqObj = {
	    op: 'create',
		id: data.node.id,
		text: data.node.text,
		parent: data.parent,
		position: data.position
	};
	$.ajax({
		type: "POST",
		url: "/admin/tree/ajax",
		data: JSON.stringify(reqObj),
		success: create_node_callback,
		contentType: 'application/json',
		dataType: "json"
	});
    }

    function rename_node(e, data) {
    	function rename_node_callback(data) {
		}
		var reqObj = {
		    op: 'rename',
			text: data.text,
			id: data.node.id
		};
		$.ajax({
			type: "POST",
			url: "/admin/tree/ajax",
			data: JSON.stringify(reqObj),
			success: rename_node_callback,
			contentType: 'application/json',
			dataType: "json"
		});
    }

    function delete_node(e, data) {
		function delete_node_callback(cb_data) {
			data.instance.refresh();
		}

		var reqObj = {
		    op: 'delete',
			id: data.node.id
		};
		$.ajax({
			type: "POST",
			url: "/admin/tree/ajax",
			data: JSON.stringify(reqObj),
			success: delete_node_callback,
			contentType: 'application/json',
			dataType: "json"
		});
    }


    function move_node(e, data) {
		function move_node_callback(data) {
		}
		var reqObj = {
			op: 'move',
			old_parent: data.old_parent,
			parent: data.parent,
			position: data.position,
			old_position: data.old_position,
			id: data.node.id
		};
		$.ajax({
			type: "POST",
			url: "/admin/tree/ajax",
			data: JSON.stringify(reqObj),
			success: move_node_callback,
			contentType: 'application/json',
			dataType: "json"
		});
    }

    function copy_node(e, data) {
        //console.log(data);
    }

    tree.on('activate_node.jstree', activate_node);
    tree.on('create_node.jstree', create_node);
    tree.on('rename_node.jstree', rename_node);
    tree.on('delete_node.jstree', delete_node);
    tree.on('move_node.jstree', move_node);
    tree.on('copy_node.jstree', copy_node);
    tree.on('state_ready.jstree', function(e,data) { 
	$('#event_result').load('/concepts/view/' + $("#concepts_tree").jstree(true).get_selected()[0]) 
    });
    


});
