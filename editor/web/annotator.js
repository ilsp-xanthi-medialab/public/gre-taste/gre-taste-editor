$( "#searchForm" ).submit(function( event ) {

	event.preventDefault();
 	var $form = $( this ),
	term = $("#comment").val();
			term = term.replace(/\n/gm, " ");
			term = term.replace(/\r/gm, " ");
			term = term.replace(/&/gm, " ");
			term = term.replace(/\"/gm, " ");
			term = term.replace(/</gm, " ");
			term = term.replace(/>/gm, " ");
	url = "http://food.ioperm.org/food-automaton-1.0.0/search/booking";

	$.ajax({
		url:url,
		type:"POST",
		data:JSON.stringify(term),
		contentType:"application/json; charset=utf-8",
		dataType:"json",
		success: function(result) {

			result.sort((a, b) => (a.off > b.off) ? 1 : (a.off === b.off) ? ((a.len > b.len) ? 1 : -1) : -1 )


			$("#result").empty();

			$('#result').append("<h2>Results</h2>");

			var content = "<table class=\"table\">"
			content += '<tr><td>Concept</td><td>Position</td></tr>' ;
			for(var k in result) {
				content += '<tr>';
				content += '<td><a href="/admin/?action=show&entity=Concept&id='+result[k].id+'">' + term.substr(result[k].off-1, result[k].len-result[k].off) + '</a></td>';
				content += '<td>' + result[k].off + ',' + result[k].len + '</td>';
				content += '</tr>';
			}
			content += "</table>"
			$('#result').append(content);
		}
	})
});

