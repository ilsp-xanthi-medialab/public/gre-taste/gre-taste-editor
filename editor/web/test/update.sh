#!/bin/bash
BACKUP_DATE=`date +%Y%m%d%H%M%S`
cd /var/www/gretaste/web/test

/usr/bin/php scorpus.ru.php > corpus.spell.ru.$BACKUP_DATE.html
cp corpus.spell.ru.$BACKUP_DATE.html corpus.spell.ru.html

/usr/bin/php scorpus.php > corpus.spell.en.$BACKUP_DATE.html
cp corpus.spell.en.$BACKUP_DATE.html corpus.spell.en.html

/usr/bin/php corpus.ru.php > corpus.ru.$BACKUP_DATE.html
cp corpus.ru.$BACKUP_DATE.html corpus.ru.html

/usr/bin/php corpus.php > corpus.en.$BACKUP_DATE.html
cp corpus.en.$BACKUP_DATE.html corpus.en.html


