<?php

$table = "<html>"
        . "<head>"
        . "<meta charset=\"UTF-8\">\n"
        . "<style>table, th, td {\n"
        . "border: 1px solid black;\n"
        . "padding: 2px 4px 2px 8px;\n"
        . "border-collapse: collapse;\n"
        . "}\n"
        . "</style>"
        . "</head>"
        . "<body>";
echo $table;



$o = json_decode(file_get_contents("corpus.json"));
foreach ($o as $e) {

    $table = sprintf("<h2>%s</h2>\n", htmlspecialchars($e->origin));
    echo $table;

    $parts = [];
    $parts[] = $e->c;
    if (mb_strlen($e->c) > 1800) {
        $parts = mb_wordwrap_array($e->c, 1500);

    $table = sprintf("<p style=\"color:red;\">%s</p>\n", "document is too big. It will be splitted.");
    echo $table;


    }



    //print_r($parts);
    foreach ($parts as $content) {
        //continue;
        $tr = file_get_contents('http://gretaste.ilsp.gr/gretaste-service/translate2?locale=ru', false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => "Content-type: text/plain",
                'content' => $content
            ]
        ]));

        $table = sprintf("<p>%s</p>\n", nl2br(htmlspecialchars($content)));


        $obj = json_decode($tr);

        $table .= "<table>";
        $table .= "<tr>";
        $table .= sprintf("<th>#</th>");
        $table .= sprintf("<th>Lemma</th>");
        $table .= sprintf("<th>Translated</th>");
        $table .= sprintf("<th>Dictionary</th>");
        $table .= sprintf("<th>MWE</th>");
        $table .= sprintf("<th>MWE Size</th>");
        $table .= sprintf("<th>Entity</th>");
        $table .= sprintf("<th>Word</th>");
        $table .= sprintf("<th>Translation</th>");
        $table .= "</tr>\n";

        $i = 0;
        $j = 0;
        foreach ($obj as $row) {
            $table .= "<tr>";
	    $prefix = "";
            $ids ="";
	    foreach($row->entity_ids as $row_entity_id) {
                $ids .= sprintf("%s<a href=\"http://gretaste.ilsp.gr/admin/concepts/tree?id=%d\">%d</a>", $prefix, $row_entity_id, $row_entity_id);
                $prefix = ", ";
	    }
            $m = '';
            if ($row->startOfMW) {
                $m = "1st";
            }
            if ($row->partOfMW) {
                $m = "+";
            }
            $lexName = ($row->lexicon === 0 ? 'None' : ($row->lexicon === 1 ? 'GreTaste' :
                    ($row->lexicon === 3 ? 'Generic' : ($row->lexicon === 2 ? 'Override' : $row->lexicon))));

            $table .= sprintf("<td>%s</td>", $i);
            $table .= sprintf("<td>%s</td>", htmlspecialchars($row->lemma));
            $table .= sprintf("<td>%s</td>", $row->translated ? "YES" : "NO");
            $table .= sprintf("<td>%s</td>", $lexName);
            $table .= sprintf("<td>%s</td>", $m);
            $table .= sprintf("<td>%s</td>", $row->mwSize);
            $table .= sprintf("<td>%s</td>", $ids);
            $table .= sprintf("<td>%s</td>", htmlspecialchars($row->wf));
            $table .= sprintf("<td>%s</td>", htmlspecialchars($row->translation));

            $table .= "</tr>\n";
            $i++;
        }
        $table .= "</table>";
        echo $table;
    }
}
$table = "</body></html>\n";
echo $table;

function mb_wordwrap_array($string, $width) {
    if (($len = mb_strlen($string, 'UTF-8')) <= $width) {
        return array($string);
    }

    $return = array();
    $last_space = FALSE;
    $i = 0;

    do {
        if (mb_substr($string, $i, 1, 'UTF-8') == ' ') {
            $last_space = $i;
        }

        if ($i > $width) {
            $last_space = ($last_space == 0) ? $width : $last_space;

            $return[] = trim(mb_substr($string, 0, $last_space, 'UTF-8'));
            $string = mb_substr($string, $last_space, $len, 'UTF-8');
            $len = mb_strlen($string, 'UTF-8');
            $i = 0;
        }

        $i++;
    } while ($i < $len);

    $return[] = trim($string);

    return $return;
}
